# Tips de rendimiento

El sistema está pensado para llevar a cabo consultas de una gran cantidad de registros, pero si por algún motivo el sistema desarrollado almacenara cantidades groseras de tuplas en la base de datos, es posible realizar algunas acciones para mitigar problemas de rendimiento.

## Particionar tablas
El CMS es lo suficientemente inteligente para detectar tablas particionadas en MySQL, así que es posible particionar las tablas que posean un ID Primary Key (Exceptuando las tablas propias del CMS). Esta acción se puede llevar a cabo de manera manual, ingresando al configurador, y ejecutando el optimizador de tablas.

 **Nota**: *Al realizar esta acción, el sistema entra al modo **[mantenimiento](modo_mantenimiento.md)** para así bloquear el sistema y prevenir errores en inserciones de nuevas tuplas*

*No es necesaria esta acción, excepto si los registros de determinada tabla superan cantidades de varios cientos de miles de registros, y si el sistema tarda más de lo normal en cargar determinadas tablas.*
