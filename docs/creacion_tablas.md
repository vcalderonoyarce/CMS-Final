# Creación de tablas en el dashboard

## Creación de una tabla física y en el CMS:

Para crear una tabla dentro del CMS, debes crear primeramente la tabla dentro de la base de datos con los datos requeridos (Obligatoriamente con un campo *Primary key* llamado id (Sin mayúsculas)).

Una vez realizada esta acción, deberás crear un registro dentro de la tabla *cms_tablas* con la siguiente información:

| nombre_real    | nombre_plural | nombre_singular | mostrar_en_sidebar | registro_unico | por_usuario | filtro_query | icono_sidebar | padre_sidebar_id | inhabilitar_click_sidebar
| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |
| 'articulos'  | 'artículos'  | 'artículo' | 1 | 0 | 0 | null | 'fa-shopping-bag' | null | null

### Explicación de cada dato:

* **nombre_real**: Corresponde al nombre de la tabla (física), debe existir en la base de datos. **Obligatorio**
* **nombre_plural**: Corresponde al nombre del elemento en plural (Ej: Entradas, Pedidos, Artículos, etc.) **Obligatorio**
* **nombre_singular**: Es el nombre de un elemento en singular (Ej: Entrada, Pedido, Artículo, etc.) **Obligatorio**
* **mostrar_en_sidebar**: Permite definir si la tabla se mostrará en la barra lateral o no (1 para activado, 0 o null para desactivado)
* **registro_unico**: Te permite convertir la tabla en un sistema de un único registro (1 para activado, 0 o null para desactivado). Esto es útil si deseas crear una opción como ajustes únicos (Salta directamente al editor al ingresar a la tabla en lugar de crear una lista).
* **por_usuario**: Permite que los registros de cada tabla sean por usuario (Si un usuario crea registros, otros usuarios no podrán ver los suyos, y se separarán) * Requiere el campo en la tabla **cms_usuarios_id** *
* **filtro_query**: Te permite personalizar el final de la consulta
* **icono_sidebar**: Corresponde al icono que se visualizará en la sidebar (Utiliza iconos de **[Font Awesome](http://fontawesome.io/icons/)**)
* **padre_sidebar_id**: Convierte la tabla en una hija de una tabla padre, el ID debe coincidir con el ID de una tabla ya ingresada previamente.
* **inhabilitar_click_sidebar**: Útil para convertir un elemento del menú de la sidebar en un elemento sin acción, útil si deseas crear una tabla sólo para categorizar.

**Importante** : Al crear una tabla, el sistema será capaz de identificarla, pero para que el usuario tenga acceso, y se muestre dentro de la barra lateral, será necesario **[establecer los permisos según nivel](permisos_tablas.md)**.
