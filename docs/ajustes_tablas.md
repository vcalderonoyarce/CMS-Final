# Ajustes de tablas y sus campos:

## Campos requeridos:

Para crear campos obligatorios en una tabla ya definida, deberás crear un ajuste en la tabla **cms_opciones_tablas** con la siguiente información:

| cms_tablas_id  | ajuste | valor_ajuste
| ------------- | ------------- | ------------- |
| 1  | campos_requeridos  | campo_1, campo_2, campo_3
| 35  | campos_requeridos  | campo_1, campo_2, campo_3

## Campos ocultos (Lista):

Para crear campos ocultos dentro de una lista en una tabla ya definida, deberás crear un ajuste en la tabla **cms_opciones_tablas** con la siguiente información:

| cms_tablas_id  | ajuste | valor_ajuste
| ------------- | ------------- | ------------- |
| 1  | campos_ocultos_lista  | campo_1, campo_2, campo_3
| 35  | campos_ocultos_lista  | campo_1, campo_2, campo_3

## Campos ocultos (Editor):

Para crear campos ocultos dentro del editor en una tabla ya definida, deberás crear un ajuste en la tabla **cms_opciones_tablas** con la siguiente información:

| cms_tablas_id  | ajuste | valor_ajuste
| ------------- | ------------- | ------------- |
| 1  | campos_ocultos_editor  | campo_1, campo_2, campo_3
| 35  | campos_ocultos_editor  | campo_1, campo_2, campo_3

## Tipos de campos

:::::pendiente:::::

## Pestañas dentro del Editor

Para agregar pestañas y organizar los campos dentro del editor, debes crear un ajuste en la tabla **cms_opciones_tablas** con la siguiente información:

| cms_tablas_id  | ajuste | valor_ajuste
| ------------- | ------------- | ------------- |
| 1  | tabs  | {"Pestaña 1":["campo_1","campo_2","campo_3"],"Pestaña 2":["campo_4","campo_5"]}

La estructura del código de la pestaña corresponde a un texto en formato JSON. Es un array multidimensional asociativo el cual utiliza el nombre de la pestaña como index key, y cada elemento dentro del array, es un campo asociado a dicha pestaña.

*Nota: Los campos que no sean agregados, irán directamente a la pestaña 'General'.*

## Campos de sólo lectura

Para definir si determinados campos dentro de una tabla son accesibles únicamente como modo lectura, es necesario crear el siguiente ajuste dentro de la tabla **cms_opciones_tablas**:

| cms_tablas_id  | ajuste | valor_ajuste
| ------------- | ------------- | ------------- |
| 1  | campos_solo_lectura  | campo_1, campo_2, campo_3
| 35  | campos_solo_lectura  | campo_1, campo_2, campo_3
