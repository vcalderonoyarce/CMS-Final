# Configuración inicial

La configuración inicial del sistema administrativo de contenido consta de 2 pasos:

## 1.Configurar conexión a la base de datos

Dentro del archivo **config.php** ubicado en **admin/** se encuentran las constantes de conexión del sistema y las rutas que deberán ser prestablecidas.

Para que el sistema funcione, la base de datos debe existir previamente.

## 2.Configuración mediante el asistente

Si el sistema no encuentra las tablas escenciales dentro de la base de datos, no funcionará correctamente. El asistente de configuración le permite realizar una instalación limpia del CMS a partir de cero, en donde se encargará de instalar todas las tablas dentro de la base de datos, te permitirá además configurar cada panel de administración, gestionar usuarios, .etc.

Para ejecutar el asistente ve a admin/install.php

*Nota: Es importante saber, que el archivo install.php debe ser removido en un entorno de producción, ya que cualquier usuario puede vaciar toda la configuración inicial y causar estragos en el sitio al acceder mediante la ruta de instalación.*
