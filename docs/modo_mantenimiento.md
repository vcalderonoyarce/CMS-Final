# Entrar en modo mantenimiento

El sistema permite inhabilitar toda actividad dentro del sistema, la función fue creada para realizar labores de mantenimiento, actualización, etc.

Para deshabilitar el sistema de manera temporal, deberás crear un archivo dentro de la carpeta admin/ llamado **.maintenance**

Nota: *Hay algunos módulos (Tales como el módulo de transacciones mediante **TransBank WebPay**) que no deshabilitan el sitio en determinados escenarios (Por ejemplo, si un usuario está realizando un pago mediante TransBank, se realizará toda la ejecución hasta llegar a su final).*
