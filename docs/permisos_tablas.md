# Establecer permisos de cada tabla según el perfil de cada usuario:

El sistema de permisos del CMS trabaja mediante niveles o perfiles, para agregar niveles deberás crear una nueva tupla en la tabla **cms_tablas_perfiles** con la siguiente información:

| cms_perfiles_id | cms_tablas_id | puede_visualizar | puede_agregar | puede_modificar | puede_eliminar
| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |
| 2 | 25 | 1 o 0/null | 1 o 0/null| 1 o 0/null | 1 o 0/null

Recordando y comprendiendo que **cms_perfiles_id** corresponde al ID del perfil dentro del sistema, y **cms_tablas_id** es el ID de la tabla a la cual se le darán permisos.
