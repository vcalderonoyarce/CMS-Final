<?php

  // Clase para renderizar una paginación a partir de 2 parámetros...
  class Paginacion {
    function renderPaginacion($paginas = 0,$pagina = 0, $ipp = 10,$url_query) {
      $ipp = ($ipp != null) ? $ipp : 10;
      $r = null;
      $links = '';
      if($paginas > 1) {
                  if ($paginas > 0 && $pagina <= $paginas) {
                    if($pagina != 1) {
                       $links .= '<a href="'.$url_query.'?pagina='.($pagina-1).'&ipp='.$ipp.'" class="anim2x nav icon-back"><i class="fas fa-angle-left"></i></a>';
                    }
                    if($pagina == 1) {
                      $links .= "<a class=\"anim2x active\" href=\"javascript:void(0);\">1</a>";
                    }
                    else {
                      $links .= "<a class=\"anim2x\" href=\"{$url_query}?pagina=1&ipp={$ipp}\">1</a>";
                    }

                  $i = max(2, $pagina - 5);
                  if ($i > 2)
                  $links .= "<a href=\"javascript:void(0)\" class=\"puntos\">...</a>";
                 for (; $i < min($pagina + 6, $paginas); $i++) {
                   if($pagina != $i){
                       $links .= "<a class=\"anim2x\" href=\"{$url_query}?pagina={$i}&ipp={$ipp}\">{$i}</a>";
                   }
                     else {
                       $links .= "<a class=\"anim2x active disable-events\" href=\"{$url_query}?pagina={$i}&ipp={$ipp}\">{$i}</a>";
                     }
                   }
                   if ($i != $paginas)
                      $links .= "<span class=\"dots\">...</span>";
                       if($pagina != $i) {
                         $links .= "<a class=\"anim2x\" href=\"{$url_query}?pagina={$paginas}&ipp={$ipp}\">$paginas</a>";
                       }
                       else {
                         $links .= "<a class=\"anim2x active disable-events\" href=\"{$url_query}?pagina={$paginas}&ipp={$ipp}\">$paginas</a>";
                       }
                     }
                   }
                  if($pagina != $paginas) {
                    $links.= '<a class="anim2x last nav n icon-next" href="'.$url_query.'?pagina='.($pagina+1).'&ipp='.$ipp.'"><i class="fas fa-angle-right"></i></a>';
                  }
                  if($pagina < $paginas) {
                    $links.= '<a href="'.$url_query.'?pagina='.($paginas).'&ipp='.$ipp.'" class="anim2x last nav icon-last"><i class="fas fa-angle-double-right"></i></a>';
                  }
      $r = strlen(trim($links)) > 0 ? $links : null;
      return $r;
    }
  }
?>
