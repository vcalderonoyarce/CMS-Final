<?php
class Plantilla {
    public $_file;
    public $nivel_base;
    public $_data = array();
    public function __construct($file = null)  {
      global $nivel_base;
        $this->nivel_base = $nivel_base;
        $this->_file = $this->nivel_base.$file;
    }
    public function set($key, $value) {
        $this->_data[$key] = $value;
        return $this;
    }
    public function render() {
        if(file_exists($this->_file)) {
          extract($this->_data);
          ob_start();
          require($this->_file);
          return ob_get_clean();
        }
        else {
          guardarLog('Se está intentando cargar una vista la cual no existe en el sistema',true);
          return false;
        }
    }
}
?>
