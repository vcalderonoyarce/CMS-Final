<?php

/*
Clase para cifrar datos sensibles, validarlos, etc.
BASADO EN PASSWORD HASH
*/
class Encryption{

    public $secret_key = 'my_simple_secret_key';
    public $secret_iv = 'my_simple_secret_iv';
    public $encrypt_method = "AES-256-CBC";
    public $key = null;
    public $iv = null;

    function __construct(){
        $this->key = hash( 'sha256', $this->secret_key);
        $this->iv = substr( hash( 'sha256', $this->secret_iv ), 0, 16 );
    }



    public function hash($val){
        $encrypted = password_hash($val, PASSWORD_DEFAULT);
        return $encrypted;
    }

    public function checkHash($string,$encrypted){
        return password_verify($string, $encrypted);
    }

    function encrypt($string) {
        $res = null;
        $res = base64_encode( openssl_encrypt( $string, $this->encrypt_method, $this->key, 0, $this->iv ) );
        return $res;
    }

    function decrypt($string){
        $res = null;
        $res = openssl_decrypt( base64_decode( $string ), $this->encrypt_method, $this->key, 0, $this->iv );
        return $res;
    }

}

$encrypt = new Encryption();
/*var_dump($encrypt->hash('test')); // Hashear una contraseña
var_dump($encrypt->checkHash('test','$2y$10$tkv7F3VDytonsm7vvLgCuu3AHKDrMziDu7FUhYONwgsDyBuJ8ZUou')); // Comprobar una contraseña
var_dump($encrypt->encrypt('4594439577977162')); // Encriptar una string...
var_dump($encrypt->decrypt('QXBkRzd1Ym1zZDBKU1dRWGRoOVFwZz09')); // Desencriptar una string...*/

?>
