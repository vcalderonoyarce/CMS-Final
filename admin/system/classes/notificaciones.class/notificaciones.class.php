<?php
class Notificaciones {

  public function cargarNotificaciones($user = null,$estado = null) {
    $r = null;
    if(isset($user)) {
      $usr = new Usuario();
      $datos = $usr->obtenerDatos($user);
      if(is_array($datos)) {
        $perfil = $datos['id_perfil'];
        $id_usuario = $user;
        // Consulto todas las notificaciones
        $db = new Database();
        $db->prepareQuery('SELECT
          cms_notificaciones.id,
          cms_notificaciones.nombre,
          cms_notificaciones.texto,
          cms_notificaciones.url_thumbnail,
          cms_notificaciones.color_etiqueta,
          cms_notificaciones.fecha_creacion,
          cms_estados_notificaciones.leido
          FROM
          cms_notificaciones
          INNER JOIN cms_notificaciones_perfiles ON cms_notificaciones_perfiles.cms_notificaciones_id
          LEFT JOIN cms_estados_notificaciones ON cms_estados_notificaciones.cms_notificaciones_id = cms_notificaciones.id
          WHERE cms_notificaciones_perfiles.cms_perfiles_id = ? AND cms_notificaciones.fecha_programacion < NOW() GROUP BY cms_notificaciones.id ORDER BY cms_notificaciones.fecha_creacion DESC',[$perfil]);
          if($estado == 'pendientes') {
            $db->addWhere('(cms_estados_notificaciones.leido IS NULL OR cms_estados_notificaciones.leido = 0)');
          }
          if($estado == 'leidos') {
            $db->addWhere('cms_estados_notificaciones.leido = 1');
          }
          $notificaciones = $db->execQuery();
          if($notificaciones == null) {
            // Escribir log
          }
          if(is_array($notificaciones)) {
            $r = $notificaciones;
            foreach($r as $key => $notificacion) {
              $r[$key]['leido'] = boolval($notificacion['leido']);
              $r[$key]['fecha_creacion'] = (strtotime($notificacion['fecha_creacion']) != false) ? new Datetime($notificacion['fecha_creacion']) : NULL;
            }
          }
      }
    }
    return $r;
  }

  function marcarLeido($id = null) {
    $r = null;
    if($id != null && isset($_SESSION['cms_user'])) {
      $db = new Database();
      $delete = $db->runDelete('DELETE FROM cms_estados_notificaciones WHERE cms_notificaciones_id = ? AND cms_usuarios_id = ?',[$id,$_SESSION['cms_user']]);
      $insert = $db->runInsert('INSERT INTO cms_estados_notificaciones (cms_notificaciones_id, cms_usuarios_id, leido) values (?,?,?)',[$id,$_SESSION['cms_user'],1]);
    }
    return $r;
  }

}
