<?php class Database {

  /**
   * Clase para conectar a MySQL
   *
   * @author Víctor Calderón Oyarce <vcalderonoyarce@gmail.com>
   * @version 1.0
   */


   protected $dbhost = dbhost;
   protected $dbuser = dbuser;
   protected $dbpass = dbpassword;
   protected $dbname = dbname;
   protected $con;
   public $current_query = null;
   public $debug = true; // Cambiar a true para devolver los errores de las consultas...

   // Inicia una conexión
  public function connect() {
    $res = true;
    $this->con = new mysqli($this->dbhost, $this->dbuser,$this->dbpass,$this->dbname);
    mysqli_set_charset($this->con, 'utf8'); // Fix (UTF8 encoding)
      if($this->con->connect_errno) {
        $res = false;
      }
      return $res;
  }

  // Cierra la conexión
  public function disconnect(){
    $this->con->close();
  }

  // Se prepara una consulta pero no se envía...
  public function prepareQuery($string,$array_fields = []) {
    $this->connect();
    $res = false;
    $array_protegido = [];
    foreach($array_fields as $item){
      $array_protegido[] = $this->con->real_escape_string($item);
    }
    $this->disconnect();
    $str_explode = explode('?',$string);
    $query_string = '';
    foreach ($str_explode as $key => $item){
      if($item != '') { // Fix para el delimitador vacío...
        $query_string.= (isset($array_protegido[$key])) ? $item.$array_protegido[$key] : $item;
          //$query_string.= $item.$array_protegido[$key];
      }
    }
    $this->current_query = $query_string;
    $res = true;
    return $res;
  }

  // Para agregar columnas en una consulta...
  public function addColumn($string = null) {
    $res = false;
    $query_string = $this->current_query;
    if($query_string != null) {
      $explode_from = explode(' FROM ',$query_string);
      if(count($explode_from) > 1) {
        $explode_from[0].= ', '.$string.' FROM ';
      }
      else { // Si no fue definido un WHERE antes...
        $explode_from[0].= 'SELECT '.$string.' FROM ';
      }
      $implode = implode(' ', $explode_from);
      $this->current_query = $implode;
      $res = true;
    }
    else {
      $res = null;
    }
    $this->current_query = preg_replace('/\s+/', ' ',$this->current_query);
    return $res;
  }


  // Para añadir un WHERE a la consulta...
  public function addWhere($string = null) {
    $res = false;
    $query_string = $this->current_query;
    if($query_string != null) {
      $explode_where = explode('WHERE ',$query_string);
      if(count($explode_where) > 1) {
        $explode_where[0].= ' WHERE '.$string.' AND ';
      }
      else { // Si no fue definido un WHERE antes...
        $explode_where[0].= ' WHERE '.$string;
      }
      $implode = implode(' ', $explode_where);
      $this->current_query = $implode;
      $res = true;
    }
    else {
      $res = null;
    }
    $this->current_query = preg_replace('/\s+/', ' ',$this->current_query);
    return $res;
  }

  // Te permite agregar GROUPS, ORDER BY
  public function endQuery($string = null) {
    $res = false;
    $query_string = $this->current_query;
    if($query_string != null) {
      $query_string.= ' '.$string;
      $this->current_query = $query_string;
    }
    return $res;
  }

  // Para añadir un límite en la consulta almacenada..
  public function limit($limit = null){
    $res = false;
    $query_string = $this->current_query;
    if($query_string != null) {
      $explode_limit = explode('LIMIT ',$query_string);
      $explode_limit[0].= ' LIMIT '.$limit;
      $implode = $explode_limit[0];
      $this->current_query = $implode;
      $res = true;
    }
    else {
      $res = null;
    }
    $this->current_query = preg_replace('/\s+/', ' ',$this->current_query);
    return $res;
  }


  public function execQuery() {
    if($this->current_query == null) {
      $res = false;
    }
    else {
      $this->connect();
      // $this->current_query = $this->con->real_escape_string($this->current_query); // Sanitizo...
      $query = $this->con->query($this->current_query);
      if($query == false) {
        if($this->debug == true) {
          echo '<div style="padding:5px;background:rgba(255,0,0,.25);font-size:"><b>Se ha encontrado un error MySQL: </b><br/><i>'.mysqli_error($this->con).'</i></div>';;
        }
        $res = null;
      }
      else {
        $rowx = [];
        while($row = mysqli_fetch_assoc($query)){
          $rowx[] = $row;
        }
        if(count($rowx) == 0) {
          $res = -1;
        }
        else {
          $res = $rowx;
        }
        mysqli_free_result($query);
      }
      $this->disconnect();
      return $res;
    }
  }


  public function countRows($string,$array_fields = []) {

  }


  public function runQuery($string,$array_fields = []){
    $res = false;
    // Conectamos...
    $this->connect();
    // Sanitizamos los datos que se traigan...
    $array_protegido = [];
    foreach($array_fields as $item){
      $array_protegido[] = $this->con->real_escape_string($item);
    }

    $str_explode = explode('?',$string);
    $query_string = '';
    foreach ($str_explode as $key => $item){
      if($item != '') { // Fix para el delimitador vacío...
        $query_string.= (isset($array_protegido[$key])) ? $item.$array_protegido[$key] : $item;
        //$query_string.= $item.$array_protegido[$key];
      }
    }
    $query = $this->con->query($query_string);
    if($query == false) {
      if($this->debug == true) {
        echo '<div style="padding:5px;background:rgba(255,0,0,.25);font-size:"><b>Se ha encontrado un error MySQL: </b><br/><i>'.mysqli_error($this->con).'</i></div>';;
      }
      $res = null;
    }
    else {
      $rowx = [];
      while($row = mysqli_fetch_assoc($query)){
        $rowx[] = $row;
      }



      if(count($rowx) == 0) {
        $res = -1;
      }
      else {
        $res = $rowx;
      }

      mysqli_free_result($query);
    }


    $this->disconnect();
    return $res;
  }
  public function runUpdate($string,$array_fields) {
    $res = false;

    // Conectamos...
    $this->connect();
    // Sanitizamos los datos que se traigan...
    $array_protegido = [];
    foreach($array_fields as $item){
      $array_protegido[] = $this->con->real_escape_string($item);
    }

    $str_explode = explode('?',$string);
    $query_string = '';
    foreach ($str_explode as $key => $item){
      if($item != '') { // Fix para el delimitador vacío...
          $query_string.= $item.$array_protegido[$key];
      }
    }
    $query = $this->con->query($query_string);
    if($query == true) {
      $res = true;
    }
    else {
      if($this->debug == true) {
        echo '<div style="padding:5px;background:rgba(255,0,0,.25);font-size:"><b>Se ha encontrado un error MySQL: </b><br/><i>'.mysqli_error($this->con).'</i></div>';;
      }
      $res = false;
    }
    $this->disconnect();
    return $res;
  }

  public function runInsert($string,$array_fields) {
    $this->connect();
    // Sanitizamos los datos que se traigan...
    $array_protegido = [];
    foreach($array_fields as $item){
      $array_protegido[] = $this->con->real_escape_string($item);
    }

    $str_explode = explode('?',$string);
    $query_string = '';
    foreach ($str_explode as $key => $item){
      if($item != '') { // Fix para el delimitador vacío...
          if(isset($array_protegido[$key]) && $key < count($str_explode) - 1) {
            if($array_protegido[$key] != null) {
              $query_string.=  $item.'NULL';
            }
          }
          else {
            $query_string.= (isset($array_protegido[$key])) ? $item.$array_protegido[$key] : $item;
          }
      }
    }
    $query_string = str_replace("'NULL'","NULL",$query_string); // Reemplazo los nulls literales a un valor NULL para la query...
    $query = $this->con->query($query_string);
    if($query == false) { // Si no se ejecuta..
      if($this->debug == true) {
        echo '<div style="padding:5px;background:rgba(255,0,0,.25);font-size:"><b>Se ha encontrado un error MySQL: </b><br/><i>'.mysqli_error($this->con).'</i></div>';;
      }
      $res = null;
    }
    else {
      $res = $this->con->insert_id;
    }
    return $res;
  }


  public function runDelete($string,$array_fields) {
    return $this->runUpdate($string,$array_fields);
  }


  public function addColumnInTable($tablename = null, $columnname = null, $type = null) {
    $r = null;
    $type = ($type == null) ? 'INT NULL' : $type; // Por defecto agrega un entero null
    if($tablename != null && $columnname != null) {
      $existe = $this->runQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '?' AND TABLE_NAME = '?' AND COLUMN_NAME = '?'",[$this->dbname, $tablename,$columnname]);
      if(!is_array($existe)) { // Si no existe...
        $actualizar = $this->runUpdate("ALTER TABLE ? ADD ? ?",[$tablename,$columnname,$type]);
      }
    }
  }
}

?>
