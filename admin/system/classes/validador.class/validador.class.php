<?php


class Validador {

  public function validarRut($rut){
	  $rut_sin_puntos = str_replace('.', "", $rut);
	 if (!preg_match("/^[0-9]+-[0-9kK]{1}/",$rut_sin_puntos)) {
		  return false;
  	}
  	$numerosentrada = explode("-", $rut_sin_puntos);
  	$verificador = $numerosentrada[1];
  	$numeros = strrev($numerosentrada[0]);
  	$count = strlen($numeros);
  	$count = $count -1;
  	$suma = 0;
  	$recorreString = 0;
  	$multiplo = 2;
  	for ($i=0; $i <=$count ; $i++) {
    	$resultadoM = $numeros[$recorreString]*$multiplo;
    	$suma = $suma + $resultadoM;
    	if ($multiplo == 7) {
    		$multiplo = 1;
    	}
    	$multiplo++;
    	$recorreString++;
  	}
  	$resto = $suma%11;
  	$dv= 11-$resto;
  	if ($dv == 11) {
  		$dv = 0;
  	}
  	if ($dv == 10) {
  		$dv = 'K';
  	}
  	if ($verificador == $dv) {
  		return true;
  	}else {
  		return false;
  	}
  }

  public function validarCorreo($input){
    return filter_var($input,FILTER_VALIDATE_EMAIL);
  }

  public function validarTelefono($input) {
    $regex = '/^((\+?34([ \t|\-])?)?[9|6|7]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/';
    if(preg_match($regex, $input)) {
      return true;
    }
    else{
      return false;
    }
  }

  public function validarNombre($input) {

  }

}
?>
