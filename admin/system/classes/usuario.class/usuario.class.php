<?php


  class Usuario extends Database {

    public function usuarioActivo() {
      return isset($_SESSION['cms_user']);
    }

    public function obtenerDatos($id = null) {
      $r = null;
      $datos = $this->runQuery('SELECT id,cms_perfiles_id as id_perfil,nombre,apellido,email,telefono, IF(avatar <> \'\' AND avatar IS NOT NULL,CONCAT(\'../uploads/avatars/\',avatar),\'front/img/no-avatar.png\') as avatar,activo,fecha_creacion FROM cms_usuarios WHERE id = ?',[$id]);
      if(is_array($datos)) {
        $r = $datos[0];
        if($r['fecha_creacion'] != null && $r['fecha_creacion'] != '') {
          $r['fecha_creacion'] = new Datetime($r['fecha_creacion']);
        }
        $r['activo'] = boolval($r['activo']);
      }
      return $r;
    }

    public function nombrePerfil($id = null) {
      $r = null;
      $perfil = $this->runQuery('SELECT nombre FROM cms_perfiles WHERE id = ?',[$id]);
      if(is_array($perfil)) {
        $r = $perfil[0]['nombre'];
      }
      return $r;
    }

    public function usuarioBaneado($id = null) {

    }

  }


?>
