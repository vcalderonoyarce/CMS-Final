<?php

/*
  Clase con todos los helpers del sistema

*/

class CMS {

  public $ortografia_lista;
  public $db;

  public function __construct(){
    $this->db = new Database();
    $ortografia = $this->db->runQuery("SELECT buscar,reemplazar,codigo_idioma FROM cms_ortografia",[]);
    if(is_array($ortografia)) { // Prevengo de solicitarlo siempre... en la función de ortografía...
      $this->ortografia_lista = $ortografia;
    }
  }

  public function ortografia($buscar = null, $idioma = null) {
    $r = $buscar;
    if($buscar != null && $idioma != null) {
      $ortografia = $this->ortografia_lista;
      $buscar = trim(strtolower($buscar));
      $idioma = trim(strtolower($idioma));
      $lang_only = [];
      foreach($ortografia as $key => $ort) {
          if($ort['codigo_idioma'] == $idioma) {
            $lang[] = $ort;
          }
      }
      $ortografia = $lang;
      $explode = explode(' ',$buscar);
      if(count($ortografia) > 0) {
        $cadena_nueva = [];
        foreach($explode as $k => $palabra) {
          $key = array_search($palabra, array_column($ortografia, 'buscar'));
          $cadena_nueva[] = ($key !== false) ? $ortografia[$key]['reemplazar'] : $palabra;
        }
        $r = implode(' ',$cadena_nueva);
      }
    }
    return $r;
  }


  public function getPk($tabla = null) {
    $r = null;
    if($tabla != null) {
      $pk = $this->db->runQuery("SHOW KEYS FROM ? WHERE Key_name = 'PRIMARY'", [$tabla]);
      if(is_array($pk)) {
        $r = $pk[0]['Column_name'];
      }
    }
    return $r;
  }

  public function datosForaneos($tabla = null) {
    $r = null;
    if($tabla != null)  {
      $pk = $this->getPk($tabla);
      if($pk != null) {
        $d = $this->db->runQuery('SELECT ? as fkey, nombre FROM ?',[$pk,$tabla]);
        $r = (is_array($d)) ? $d : null;
      }
      else {
        guardarLog("La tabla '$tabla' no posee una primary key, y se está intentando acceder desde otra tabla",true);
      }
    }
    return $r;
  }

  public function tablasMenus($user_id = null,$parent_id = null) {
    $r = null;
    if($user_id != null) {
      $usuario = new Usuario();
      $datos_usuario = $usuario->obtenerDatos($user_id);
      if(is_array($datos_usuario)) {
        $id_perfil = $datos_usuario['id_perfil'];
        // Obtengo las tablas según perfil...
        if($parent_id != null) {
          $tablas = $this->db->runQuery("SELECT
              cms_tablas.id,
              cms_tablas.nombre_real,
              cms_tablas.nombre_plural,
              cms_tablas.nombre_singular,
              cms_tablas.inhabilitar_click_sidebar as desactivar_click, if(cms_tablas.icono_sidebar IS NOT NULL AND cms_tablas.icono_sidebar <> '',cms_tablas.icono_sidebar, 'fas fa-question') as icono_sidebar FROM cms_tablas INNER JOIN cms_tablas_perfiles ON cms_tablas_perfiles.cms_tablas_id = cms_tablas.id WHERE
              cms_tablas_perfiles.cms_perfiles_id = ?
              AND
              cms_tablas.padre_sidebar_id = ?
              AND
              cms_tablas.mostrar_en_sidebar = 1
              GROUP by cms_tablas.id"
              ,[$id_perfil,$parent_id]);
        }
        else {
          $tablas = $this->db->runQuery("SELECT
              cms_tablas.id,
              cms_tablas.nombre_real,
              cms_tablas.nombre_plural,
              cms_tablas.nombre_singular,
              cms_tablas.inhabilitar_click_sidebar as desactivar_click,
              if(cms_tablas.icono_sidebar IS NOT NULL AND cms_tablas.icono_sidebar <> '',cms_tablas.icono_sidebar, 'fas fa-question') as icono_sidebar
              FROM cms_tablas
              INNER JOIN cms_tablas_perfiles ON cms_tablas_perfiles.cms_tablas_id = cms_tablas.id
              WHERE
              cms_tablas_perfiles.cms_perfiles_id = ?
              AND
              cms_tablas.mostrar_en_sidebar = 1
              AND cms_tablas.padre_sidebar_id IS NULL
              GROUP by cms_tablas.id"
              ,[$id_perfil]);
        }

          if(is_array($tablas)) {
            $r = '<ul>';
            foreach($tablas as $key => $tabla) {
              // Consulto si existen hijos... se hace un bucle finito de la función mientras existan tablas hijas...
                $class = (!isset($_GET['table_id'])) ? '' : ($_GET['table_id'] == $tabla['id']) ? 'active' : '';
                $tablas[$key]['desactivar_click'] = boolval($tabla['desactivar_click']);
                $link  = ($tablas[$key]['desactivar_click'] == true) ? 'javascript:void(0);' : 'content/'.$tabla['id'].'/'.url_permalink($tabla['nombre_plural']);
                $r.= '<li class="radius">';
                $r.= '<a data-title="'.capitalizarCadena($tabla['nombre_plural']).'" class="'.$class.' radius b sidebar-link block" href="'.$link.'"><i class="valign '.$tabla['icono_sidebar'].'"></i>'.capitalizarCadena($tabla['nombre_plural']).'</a>';
                $r.= $tablas[$key]['tablas_hijas'] = $this->tablasMenus($user_id,$tabla['id']);
                $r.= '</li>';
              }
              $r.= '</ul>';
          }
      }
    }


    return $r;
  }


  public function tableisInit($id = null) {
    $r = null;
    if($id != null) {
      $nombre_tabla = $this->db->runQuery('SELECT nombre_real FROM cms_tablas WHERE id = ?',[$id]);
      $r = is_array($nombre_tabla);
    }
    return $r;
  }

  public function nombreTabla($id = null) {
    $r = null;
    if($id != null) {
        $nombre_tabla = $this->db->runQuery('SELECT nombre_real FROM cms_tablas WHERE id = ?',[$id]);
        $r = (is_array($nombre_tabla)) ? $nombre_tabla[0]['nombre_real'] : null;
    }
    return $r;
  }

  public function tableExists($id = null) {
    $r = null;
    if($id != null) {
      if($this->tableisInit($id) == true) {
        $nombre_tabla = $this->db->runQuery('SELECT nombre_real FROM cms_tablas WHERE id = ?',[$id]);
        $nombre_tabla = $nombre_tabla[0]['nombre_real'];
        $compruebo = $this->db->runQuery("SHOW TABLES LIKE '?'",[$nombre_tabla]);
        if(is_array($compruebo)) {
          $r = true;
        }
        else {
          $r = false;
        }
      }
      else {
        $r = false;
      }
    }
    return $r;
  }

  public function tienePermisosTabla($user_id = null,$table_id = null) {
    $r['puede_visualizar'] = false;
    $r['puede_agregar'] = false;
    $r['puede_modificar'] = false;
    $r['puede_eliminar'] = false;
    $r['puede_imprimir'] = false;
    $r['puede_ver_informe'] = false;
    if($user_id != null && $table_id != null) {
      if($this->tableisInit($table_id) == true) {
        $usuario = new Usuario();
        $datos_usuario = $usuario->obtenerDatos($user_id);
        if(is_array($datos_usuario)) {
          $id_perfil = $datos_usuario['id_perfil'];
          $consulto_permisos = $this->db->runQuery('SELECT puede_visualizar,puede_agregar,puede_modificar,puede_eliminar,puede_imprimir,puede_descargar,puede_ver_informe FROM cms_tablas_perfiles WHERE cms_tablas_id = ? AND cms_perfiles_id = ?',[$user_id,$id_perfil]);
          if(is_array($consulto_permisos)) {
            $r = $consulto_permisos[0];
            $r['puede_visualizar'] = boolval($r['puede_visualizar']);
            $r['puede_agregar'] = boolval($r['puede_agregar']);
            $r['puede_modificar'] = boolval($r['puede_modificar']);
            $r['puede_eliminar'] = boolval($r['puede_eliminar']);
            $r['puede_imprimir'] = boolval($r['puede_imprimir']);
            $r['puede_descargar'] = boolval($r['puede_descargar']);
            $r['puede_ver_informe'] = boolval($r['puede_ver_informe']);
          }
        }
      }
    }
    return $r;
  }

  public function ajustesGlobales() {
    $ajustes_generales = $this->db->runQuery('SELECT
      campos_checkbox,
      campos_enriquecidos,
      campos_rut,
      campos_ubicacion,
      campos_imagen,
      campos_archivo,
      campos_password,
      campos_tag,
      campos_hora,
      campos_fecha,
      campos_fecha_hora,
      campos_video_incrustado,
      campos_video,
      campos_audio,
      campos_tarjeta_credito,
      nombre_sitio,
      idioma_por_defecto
      FROM cms_ajustes_globales
      LIMIT 1
      ',[]);
      if(!is_array($ajustes_generales)) {
        guardarLog('Se produjo un error crítico al intentar obtener los ajustes del sistema',true);
      }
      else {
        $ajustes_generales = $ajustes_generales[0];
        $r['ajustes_sitio']['nombre_sitio'] = $ajustes_generales['nombre_sitio'];
        $r['ajustes_sitio']['idioma_por_defecto'] = $ajustes_generales['idioma_por_defecto'];
        $r['tipos_de_campos']['campos_checkbox'] = (strlen(trim($ajustes_generales['campos_checkbox'])) != 0) ? explode(',',$ajustes_generales['campos_checkbox']) : null;
        $r['tipos_de_campos']['campos_enriquecidos'] = (strlen(trim($ajustes_generales['campos_enriquecidos'])) != 0) ? explode(',',$ajustes_generales['campos_enriquecidos']) : null;
        $r['tipos_de_campos']['campos_rut'] = (strlen(trim($ajustes_generales['campos_rut'])) != 0) ? explode(',',$ajustes_generales['campos_rut']) : null;
        $r['tipos_de_campos']['campos_ubicacion'] = (strlen(trim($ajustes_generales['campos_ubicacion'])) != 0) ? explode(',',$ajustes_generales['campos_ubicacion']) : null;;
        $r['tipos_de_campos']['campos_imagen'] = (strlen(trim($ajustes_generales['campos_imagen'])) != 0) ? explode(',',$ajustes_generales['campos_imagen']) : null;;
        $r['tipos_de_campos']['campos_archivo'] = (strlen(trim($ajustes_generales['campos_archivo'])) != 0) ? explode(',',$ajustes_generales['campos_archivo']) : null;
        $r['tipos_de_campos']['campos_password'] = (strlen(trim($ajustes_generales['campos_password'])) != 0) ? explode(',',$ajustes_generales['campos_password']) : null;
        $r['tipos_de_campos']['campos_tag'] = (strlen(trim($ajustes_generales['campos_tag'])) != 0) ? explode(',',$ajustes_generales['campos_tag']) : null;
        $r['tipos_de_campos']['campos_hora'] = (strlen(trim($ajustes_generales['campos_hora'])) != 0) ? explode(',',$ajustes_generales['campos_hora']) : null;
        $r['tipos_de_campos']['campos_fecha'] = (strlen(trim($ajustes_generales['campos_fecha'])) != 0) ? explode(',',$ajustes_generales['campos_fecha']) : null;
        $r['tipos_de_campos']['campos_fecha_hora'] = (strlen(trim($ajustes_generales['campos_fecha_hora'])) != 0) ? explode(',',$ajustes_generales['campos_fecha_hora']) : null;
        $r['tipos_de_campos']['campos_video_incrustado'] = (strlen(trim($ajustes_generales['campos_video_incrustado'])) != 0) ? explode(',',$ajustes_generales['campos_video_incrustado']) : null;
        $r['tipos_de_campos']['campos_video'] = (strlen(trim($ajustes_generales['campos_video'])) != 0) ? explode(',',$ajustes_generales['campos_video']) : null;
        $r['tipos_de_campos']['campos_tarjeta_credito'] = (strlen(trim($ajustes_generales['campos_tarjeta_credito'])) != 0) ? explode(',',$ajustes_generales['campos_tarjeta_credito']) : null;
      }
    return $r;
  }

  public function ajustesTabla($id = null) {
    $ajustes_globales = $this->ajustesGlobales();
    if($this->tableIsInit($id) == true) {
      $ajustes_tabla = $this->db->runQuery('SELECT nombre_real, nombre_plural, nombre_singular, registro_unico, por_usuario, filtro_query FROM cms_tablas WHERE id = ?',[$id]);
      if(is_array($ajustes_tabla)) {
        $ajustes_tabla = $ajustes_tabla[0];
        $r['tabs'] = $this->tableTabs($id);
        $r['ajustes_tabla']['id_tabla'] = ($id == null) ? null : $id;
        $r['ajustes_tabla']['nombre_tabla'] = (strlen(trim($ajustes_tabla['nombre_real'])) == 0) ? null : $ajustes_tabla['nombre_real'];
        $r['ajustes_tabla']['nombre_plural'] = (strlen(trim($ajustes_tabla['nombre_plural'])) == 0) ? null : $ajustes_tabla['nombre_plural'];
        $r['ajustes_tabla']['nombre_singular'] = (strlen(trim($ajustes_tabla['nombre_singular'])) == 0) ? null : $ajustes_tabla['nombre_singular'];
        $r['ajustes_tabla']['registro_unico'] = (strlen(trim($ajustes_tabla['registro_unico'])) == 0) ? false : boolval($ajustes_tabla['registro_unico']);
        $r['ajustes_tabla']['por_usuario'] = (strlen(trim($ajustes_tabla['por_usuario'])) == 0) ? false : boolval($ajustes_tabla['por_usuario']);
        $r['ajustes_tabla']['filtro_query'] = (strlen(trim($ajustes_tabla['filtro_query'])) == 0) ? null : $ajustes_tabla['filtro_query'];
        $r['campos_tabla'] = $this->camposTabla($id);

      }
    }
    return $r;
  }

  public function tableTabs($id = null) {
    $r = null;
    if($id != null) {
      $tabs = $this->db->runQuery("SELECT valor_ajuste as tabs FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = 'tabs'",[$id]);
      if(is_array($tabs)) {
        $array_tabs = json_decode($tabs[0]['tabs'],true);
        $r =  (json_last_error() === JSON_ERROR_NONE) ? $array_tabs : null;
      }
    }
    return $r;
  }

  public function camposTabla($id = null) {
    $r = null;
    if($id != null) {
      $nombre_tabla = $this->nombreTabla($id);
      $campos_requeridos = $this->camposRequeridos($id);
      $campos_ocultos_lista = $this->camposOcultosLista($id);
      $campos_ocultos_editor = $this->camposOcultosEditor($id);
      $campos_solo_lectura = $this->camposSoloLectura($id);
      $campos = $this->db->runQuery('SHOW FULL COLUMNS FROM ?',[$nombre_tabla]);
      if(is_array($campos)) {
        // Consulto si existen ajustes especiales para los campos...
        $ajustes_campos = $this->db->runQuery("SELECT valor_ajuste as ajustes_campo FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = 'formato_campos'",[$id]);
        if(is_array($ajustes_campos)) {
          $ajustes_campos = json_decode($ajustes_campos[0]['ajustes_campo'],true);
          $ajustes_campos =  (json_last_error() === JSON_ERROR_NONE) ? $ajustes_campos : null;
        }
        foreach($campos as $key => $campo) {
          $r[$key]['nombre_campo'] = $campo['Field'];
          $r[$key]['solo_lectura'] = is_array($campos_solo_lectura) ? in_array($campo['Field'],$campos_solo_lectura) ? true : false : false;
          $r[$key]['requerido'] = is_array($campos_requeridos) ? in_array($campo['Field'],$campos_requeridos) ? true : false : false;
          $r[$key]['oculto_lista'] = is_array($campos_ocultos_lista) ? in_array($campo['Field'],$campos_ocultos_lista) ? true : false : false;
          $r[$key]['oculto_editor'] = is_array($campos_ocultos_editor) ? in_array($campo['Field'],$campos_ocultos_editor) ? true : false : false;
          $r[$key]['tipo_campo'] = $this->tipoCampo($campo['Field'],$nombre_tabla,$this->ajustesGlobales());
          $r[$key]['valor_por_defecto'] = $campo['Default'];
          $r[$key]['comentario_campo'] = $campo['Comment'];
          $r[$key]['longitud_maxima'] = (preg_match( '!\(([^\)]+)\)!', $campo['Type'], $match )) ? $match[1] : null;
          $r[$key]['ajustes_adicionales'] = (is_array($ajustes_campos) ? isset($ajustes_campos[$campo['Field']]) ? $ajustes_campos[$campo['Field']] : null : null);
          // Sobreescribo según tipo en la DB
          if($campo['Type'] == 'int(1)') {
            $r[$key]['tipo_campo'] = 'checkbox';
          }

          if($campo['Type'] == 'int(11)' && $r[$key]['tipo_campo'] != 'foreign_key') {
            $r[$key]['tipo_campo'] = 'numerico';
          }

          if($campo['Type'] == 'datetime') {
            $r[$key]['tipo_campo'] = 'fecha_completa';
          }
          if($campo['Type'] == 'date') {
            $r[$key]['tipo_campo'] = 'fecha';
          }
          if($campo['Type'] == 'time') {
            $r[$key]['tipo_campo'] = 'hora';
          }

          if($campo['Key'] == 'PRI') {
            $r[$key]['tipo_campo'] = 'primary_key';
          }
        }
      }
    }
    return $r;
  }

  public function tipoCampo($campo = null, $tabla = null, $ajustes = null) {
    // Requiere un "overrider" para cada tabla...
    $r = 'texto';
    $campo = trim($campo);
    if($campo != null && $tabla != null && $ajustes != null) {
      if(endsWith($campo,'_id') == true) {
        $r = 'foreign_key';
      }
      else {
          // Campos checkbox...
          if(is_array($ajustes['tipos_de_campos']['campos_checkbox'])) {
            if(count($ajustes['tipos_de_campos']['campos_checkbox']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_checkbox'])) {
                return 'checkbox';
              }
            }
          }
          // Campos RUT...
          if(is_array($ajustes['tipos_de_campos']['campos_rut'])) {
            if(count($ajustes['tipos_de_campos']['campos_rut']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_rut'])) {
                return 'rut';
              }
            }
          }
          // Campos enriquecidos...
          if(is_array($ajustes['tipos_de_campos']['campos_enriquecidos'])) {
            if(count($ajustes['tipos_de_campos']['campos_enriquecidos']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_enriquecidos'])) {
                return 'enriquecido';
              }
            }
          }
          // Campos ubicacion...
          if(is_array($ajustes['tipos_de_campos']['campos_ubicacion'])) {
            if(count($ajustes['tipos_de_campos']['campos_ubicacion']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_ubicacion'])) {
                return 'ubicacion';
              }
            }
          }
          // Campos imagen...
          if(is_array($ajustes['tipos_de_campos']['campos_imagen'])) {
            if(count($ajustes['tipos_de_campos']['campos_imagen']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_imagen'])) {
                return 'imagen';
              }
            }
          }
          // Campos archivo...
          if(is_array($ajustes['tipos_de_campos']['campos_archivo'])) {
            if(count($ajustes['tipos_de_campos']['campos_archivo']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_archivo'])) {
                return 'archivo';
              }
            }
          }
          // Campos hora...
          if(is_array($ajustes['tipos_de_campos']['campos_hora'])) {
            if(count($ajustes['tipos_de_campos']['campos_hora']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_hora'])) {
                return 'hora';
              }
            }
          }
          // Campos fecha...
          if(is_array($ajustes['tipos_de_campos']['campos_hora'])) {
            if(count($ajustes['tipos_de_campos']['campos_fecha']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_fecha'])) {
                return 'fecha';
              }
            }
          }
          // Campos datetime...
          if(is_array($ajustes['tipos_de_campos']['campos_fecha_hora'])) {
            if(count($ajustes['tipos_de_campos']['campos_fecha_hora']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_fecha_hora'])) {
                return 'fecha_completa';
              }
            }
          }

          // Campos video_incrustado...
          if(is_array($ajustes['tipos_de_campos']['campos_video_incrustado'])) {
            if(count($ajustes['tipos_de_campos']['campos_video_incrustado']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_video_incrustado'])) {
                return 'video_incrustado';
              }
            }
          }
          // Campos video...
          if(is_array($ajustes['tipos_de_campos']['campos_video'])) {
            if(count($ajustes['tipos_de_campos']['campos_video']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_video'])) {
                return 'video';
              }
            }
          }
          // Campos tarjetas...
          if(is_array($ajustes['tipos_de_campos']['campos_tarjeta_credito'])) {
            if(count($ajustes['tipos_de_campos']['campos_tarjeta_credito']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_tarjeta_credito'])) {
                return 'tarjeta';
              }
            }
          }

          // Campos tipo_tag
          if(is_array($ajustes['tipos_de_campos']['campos_tag'])) {
            if(count($ajustes['tipos_de_campos']['campos_tag']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_tag'])) {
                return 'tags';
              }
            }
          }
          // Campos contraseña
          if(is_array($ajustes['tipos_de_campos']['campos_password'])) {
            if(count($ajustes['tipos_de_campos']['campos_password']) > 0) {
              if(in_array($campo,$ajustes['tipos_de_campos']['campos_password'])) {
                return 'password';
              }
            }
          }
      }
    }
    return $r;
  }
  public function camposRequeridos($tabla = null) {
    $r = null;
    if($tabla != null) {
      $campos_requeridos = $this->db->runQuery('SELECT valor_ajuste as requeridos FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = \'campos_requeridos\' AND valor_ajuste IS NOT NULL AND valor_ajuste <> \'\'',[$tabla]);
      if(is_array($campos_requeridos)) {
        $campos_requeridos = explode(',',$campos_requeridos[0]['requeridos']);
        $campos_requeridos = array_filter($campos_requeridos);
        $r = array_map('trim', $campos_requeridos);
      }
    }
    return $r;
  }
  public function camposOcultosLista($tabla = null) {
    $r = null;
    if($tabla != null) {
      $campos_ocultos = $this->db->runQuery('SELECT valor_ajuste as ocultos FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = \'campos_ocultos_lista\' AND valor_ajuste IS NOT NULL AND valor_ajuste <> \'\'',[$tabla]);
      if(is_array($campos_ocultos)) {
        $campos_ocultos = explode(',',$campos_ocultos[0]['ocultos']);
        $campos_ocultos = array_filter($campos_ocultos);
        $r = array_map('trim', $campos_ocultos);
      }
    }
    return $r;
  }
  public function camposOcultosEditor($tabla = null) {
    $r = null;
    if($tabla != null) {
      $campos_ocultos = $this->db->runQuery('SELECT valor_ajuste as ocultos FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = \'campos_ocultos_editor\' AND valor_ajuste IS NOT NULL AND valor_ajuste <> \'\'',[$tabla]);
      if(is_array($campos_ocultos)) {
        $campos_ocultos = explode(',',$campos_ocultos[0]['ocultos']);
        $campos_ocultos = array_filter($campos_ocultos);
        $r = array_map('trim', $campos_ocultos);
      }
    }
    return $r;
  }

  public function camposSoloLectura($tabla = null) {
    $r = null;
    if($tabla != null) {
      $campos_ocultos = $this->db->runQuery('SELECT valor_ajuste as solo_lectura FROM cms_opciones_tablas WHERE cms_tablas_id = ? AND ajuste = \'campos_solo_lectura\' AND valor_ajuste IS NOT NULL AND valor_ajuste <> \'\'',[$tabla]);
      if(is_array($campos_ocultos)) {
        $campos_ocultos = explode(',',$campos_ocultos[0]['solo_lectura']);
        $campos_ocultos = array_filter($campos_ocultos);
        $r = array_map('trim', $campos_ocultos);
      }
    }

    return $r;
  }

  public function nombreCampoFK($nombre_campo = null, $singular = true) {
    $r = null;
    if($nombre_campo != null) {
      $nombre_tabla = substr_replace($nombre_campo,'', -strlen('_id'));
      if($singular == true) {
        $datos_tabla = $this->tablaMicroDatos($nombre_tabla);
        $r = ($datos_tabla != null) ? $datos_tabla['nombre_singular'] : singularizarCadena($nombre_tabla);
      }
      else {
        $r = $nombre_tabla;
      }
    }
    return $r;
  }


  public function datoForaneo($id_fk = null,$nombre_campo = null) { // Pendiente
    $r = null;
    if($id_fk != null && $nombre_campo != null) {
      $nombre_tabla = substr_replace($nombre_campo,'', -strlen('_id'));
      $table_pk = $this->getPk($nombre_tabla);
      $reg = $this->db->runQuery("SELECT nombre FROM ? WHERE ? = ?",[$nombre_tabla,$table_pk,$id_fk]);
      $r = ($reg == null) ? false : (is_array($reg) ? $reg[0]['nombre'] : null);
    }
    return $r;
  }

  public function tablaMicroDatos($tb = null) {
    $r = null;
    if($tb != null) {
      $datos = $this->db->runQuery("SELECT id as id_tabla,nombre_real, nombre_singular, nombre_plural FROM cms_tablas WHERE nombre_real = '?'",[$tb]);
      $r = (is_array($datos) ? $datos[0] : null);
    }
    return $r;
  }
}


?>
