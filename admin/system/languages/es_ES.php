<?php
$lang = [];
$lang['lang_prefix'] = 'es';
$lang['lang_prefix_editor'] = 'es';

$lang['cms_title'] = 'Administración del sitio web';

// Tratamientos de fechas...
$lang['months'] = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

$lang['date_format'] = 'd \d\e \{m\} \d\e Y';
$lang['full_date_format'] = 'd \d\e \{m\} \d\e Y \a\ \l\a\s H:m';

// Errores
$lang['error_connection_title'] = 'No se pudo realizar la conexión a la base de datos';
$lang['error_connection_body'] = 'Se ha producido un error al intentar realizar la conexión a la base de datos del sitio';


// Login screen
$lang['login_title'] = 'Iniciar sesión';
$lang['login_email_placeholder'] = 'Dirección de correo electrónico';
$lang['login_password_placeholder'] = 'Contraseña';
$lang['login_button_label'] = 'Iniciar sesión';
$lang['login_description'] = 'Inicia sesión con tu cuenta para continuar';
$lang['login_welcome'] = 'Bienvenid@ de nuevo';
$lang['login_password_forgot'] = '¿Olvidaste tu contraseña?';
$lang['login_password_recovery_text'] = '¿Olvidaste tu contraseña?';
$lang['login_button_recovery_label'] = 'Recuperar contraseña';
$lang['login_back_button'] = 'Volver';
$lang['login_instructions_recovery'] = 'Ingrese su dirección de correo electrónico para recibir un correo de recuperación';


// JSON's
$lang['invalid_request'] = 'Solicitud inválida';
$lang['user_not_exist'] = 'La dirección de correo no se encuentra en el sistema';
$lang['user_not_active'] = 'Este usuario se encuentra inhabilitado para ingresar en el sistema';
$lang['login_successfull'] = 'Inicio de sesión exitoso';
$lang['not_matching_password'] = 'La contraseña no coincide con la de la cuenta';

// Header
$lang['goto_website'] = 'Ir al sitio web';

$lang['user_edit_link'] = 'Modificar perfil';
$lang['user_sign_out'] = 'Cerrar sesión';
$lang['user_support'] = 'Soporte';
$lang['user_cms_edit'] = 'Configuración';
$lang['all_notification_label'] = 'Ver todas las notificaciones';
$lang['notification_label_title'] = 'Click para ver notificación';

// Content
$lang['acc_email_pl'] = 'Dirección de correo electrónico';
$lang['acc_name_pl'] = 'Nombre';
$lang['acc_lname_pl'] = 'Apellidos';
$lang['acc_phone_pl'] = 'Número de teléfono';
$lang['edit_acc_btn'] = 'Modificar cuenta';

$lang['ssl_required'] = 'Certificado SSL requerido';
$lang['hi'] = 'Hola';
$lang['good_morning'] = 'Buenos días';
$lang['good_afternoon'] = 'Buenas tardes';
$lang['good_night'] = 'Buenas noches';
$lang['not_found_title'] = 'No encontrado';
$lang['not_found_description'] = 'No fuimos capaces de encontrar lo que estabas buscando';
$lang['no_table_selected'] = 'No se seleccionó ninguna opción válida';
$lang['table_not_initialized'] = 'No se seleccionó ninguna opción válida';
$lang['table_not_created'] = 'Se produjo un error al intentar mostrar el contenido de esta página';
$lang['no_permissions_to_view'] = 'Usted no posee los suficientes permisos para acceder a esta sección';
$lang['no_permissions_to_edit'] = 'Usted no posee los suficientes permisos para acceder a esta opción';
$lang['error_table_settings'] = 'No se pudo recuperar la información y ajustes de la sección, por favor, contacte con el soporte técnico';
$lang['not_reg_yet'] = 'No se encontraron registros de ';
$lang['not_defined_pk'] = 'Se produjo un error al intentar rescatar el dato';
$lang['user_label'] = 'usuario';
$lang['delete_title'] = 'Eliminar';
$lang['edit_title'] = 'Modificar';
$lang['print_title'] = 'Imprimir';
$lang['create_title'] = 'Crear';
$lang['new_label'] = 'Crear';
$lang['edit_label'] = 'Editar';
$lang['delete_button'] = 'Eliminar';
$lang['print_button'] = 'Imprimir';
$lang['save_changes'] = 'Guardar cambios';
$lang['cancel_button'] = 'Cancelar';
$lang['credit_card_title'] = 'Las tarjetas almacenadas se encuentran en un ambiente protegido, no es posible visualizarlas por principios de seguridad';
$lang['search_pl'] = 'Buscar';
$lang['protected_content'] = 'Contenido protegido';
$lang['safe_passwords_layer'] = 'Este contenido se encuentra protegido y es imposible de visualizar';
$lang['invalid_chilean_rut'] = 'Este RUT no es válido';
$lang['download_csv_title'] = 'Haga click para descargar una copia de la información en formato CSV (Para microsoft excel)';
$lang['view_report'] = 'Ver informe';
$lang['view_report_title'] = 'Haga click para ver un informe detallado';
$lang['video_tag_html_support'] = 'Tu navegador no soporta la reproducción HTML5 de vídeos';
$lang['click_to_play_video'] = 'Haga click para reproducir este vídeo';
$lang['item_not_found'] = 'Se está intentando modificar un elemento de la lista el cual no existe o ya no se encuentra disponible';
$lang['main_tab'] = 'General';
$lang['select_option'] = 'Seleccione una opción';
$lang['ipp_label'] = 'por página';
$lang['upload_image'] = 'Seleccionar imagen';
$lang['upload_video'] = 'Seleccionar vídeo';
$lang['accepted-images'] = 'Imágenes soportadas:';
$lang['accepted_videos'] = 'Formato de vídeo soportados:';
$lang['current_image'] = 'Imagen actual';
$lang['current_video'] = 'Vídeo actual';
$lang['remove_current_img'] = 'Eliminar imagen actual';
$lang['remove_current_video'] = 'Eliminar vídeo actual';
$lang['remove_item_conf'] = '¿Realmente desea ejecutar esta acción?';
$lang['remove_item_desc'] = 'Esta acción no se puede deshacer';
$lang['done_label'] = 'Hecho';
$lang['registry_removed_succ'] = 'Registro eliminado exitosamente';
$lang['accept_button'] = 'Aceptar';
$lang['add_tags'] = 'Ingrese la palabra que desee, seguida de la tecla \'Enter\'';
$lang['select_hour'] = 'Seleccione una hora';

$lang['invalid_field_config'] = 'Error: Faltan ajustes necesarios para este tipo de campo';
$lang['error_title'] = 'Error';
$lang['fix_inputs_message'] = 'Se encontraron errores, revise la información ingresada e intente nuevamete';
$lang['choose_an_option'] = 'Seleccione';

// Actions
$lang['back_button_text'] = 'Volver';
$lang['back_home_text'] = 'Volver a la página principal';
$lang['add_button'] = 'Agregar';

// generic
$lang['success_request'] = 'Petición exitosa';

// Notifications
$lang['user_sign_in_warn'] = 'Para llevar a cabo esta acción se necesita iniciar sesión en el sistema';
$lang['no_notifications'] = 'No se encontraron notificaciones';
$lang['cant_reach_request'] = 'Se produjo un error al intentar recuperar la solicitud';
$lang['wait_loading_not'] = 'Espere mientras se cargan las notificaciones';
$lang['notifications_label'] = 'Notificaciones';




// Extra...

$lang['maintenance_title'] = 'Sitio bajo mantenimiento';
$lang['maintenance_detail'] = 'Nuestro sitio está bajo un mantenimiento programado. Tardará poco, muchas gracias por tu paciencia';
