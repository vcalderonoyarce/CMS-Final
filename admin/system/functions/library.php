<?php
/*Librería de funciones genéricas*/

if (version_compare(phpversion(), '5.5', '<')) { // Si la versión de PHP es inferior a PHP5.5
    require_once($nivel_base.'system/functions/password_compat.php'); // Se importa el paquete de compatibilidad password_                                                       // con las funciones password_XXX
}

function endsWith($string,$find){
    return substr($string, -strlen($find)) == $find ? true : false;
}

if(!function_exists('extension_archivo')) {
  function extension_archivo($file_name) {
	   return substr(strrchr($file_name,'.'),1);
  }
}

function removeChars($string,$array = []) {
  $r = $string;
  foreach($array as $k => $item) {
    $r = str_replace($item,'',$r);
  }

  return $r;
}

function replace_between($str, $needle_start, $needle_end, $replacement) {
    $pos = strpos($str, $needle_start);
    $start = $pos === false ? 0 : $pos + strlen($needle_start);

    $pos = strpos($str, $needle_end, $start);
    $end = $pos === false ? strlen($str) : $pos;

    return substr_replace($str, $replacement, $start, $end - $start);
}

if(!function_exists('singularizarCadena')) {
  function singularizarCadena($string = null) {
    $r = $string;
    if($string != null) {
      // Si termina en ___ciones....
      if(endsWith($string,'ciones')) {
        $r = substr_replace($string,'', -strlen('ciones')).'cion';
      }

      // Si termina en ___ias....
      if(endsWith($string,'ias')) {
        $r = substr_replace($string,'', -strlen('ias')).'ia';
      }

      // Si termina en ___os....
      if(endsWith($string,'os')) {
        $r = substr_replace($string,'', -strlen('os')).'o';
      }

      // Si termina en ___as....
      if(endsWith($string,'as')) {
        $r = substr_replace($string,'', -strlen('as')).'a';
      }
      // Si termina en ___os....
      if(endsWith($string,'os')) {
        $r = substr_replace($string,'', -strlen('os')).'o';
      }
      // Si termina en ___es....
      if(endsWith($string,'es')) {
        $r = substr_replace($string,'', -strlen('es')).'';
      }

    }
    return $r;
  }
}


function guardarLog($msg = null, $error = false) {
  $r = null;
  if($msg != null) {
    $db = new Database();
    if($error == true) {
      $log = $db->runInsert("INSERT INTO cms_logs (texto,fecha_creacion,error) VALUES ('?', NOW(),1)",[$msg]);
    }
    else {
      $log = $db->runInsert("INSERT INTO cms_logs (texto,fecha_creacion,error) VALUES ('?', NOW(),0)",[$msg]);
    }

    $r = $log;
  }
  return $r;
}

function url_permalink($url) {
  $url = trim($url);
  $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ','ä','ë','ï','ö','ü');
  $repl = array('a', 'e', 'i', 'o', 'u', 'n', 'A', 'E', 'I', 'O', 'U', 'N','a','e','i','o','u');
  $url = str_replace ($find, $repl, $url);
  $url = strtolower($url);
  $find = array(' ', '&', '\r\n', '\n', '+');
  $url = str_replace ($find, '-', $url);
  $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
  $repl = array('', '-', '');
  $url = preg_replace ($find, $repl, $url);
  return $url;
}

function randomString($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));
    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
}

// Crea una imagen a partir de una cadena en bas64...
function crearImagen($base64,$carpeta) {
    $extension = 'jpg';
    global $nivel_base;
    $exploded = explode(',', $base64, 2); // limit to 2 parts, i.e: find the first comma
    $encoded = $exploded[1]; // pick up the 2nd part
    $decoded = base64_decode($encoded);
    $img = imagecreatefromstring($decoded);
    if (!$img) {
        return false;
    }
    $random = randomString(30);
    if(is_dir($nivel_base.'/imagenes/'.$carpeta)) {
      mkdir($nivel_base.'/imagenes/'.$carpeta, 0777, true);
    }
    if($extension == 'png') {
      imagepng($img, $nivel_base.'/imagenes/'.$carpeta.'/img_'.$random.'.'.$extension);
    }
    if($extension == 'jpg') { // Corregir pendiente...
      imagejpeg($img, $nivel_base.'/imagenes/'.$carpeta.'/img_'.$random.'.'.$extension);
    }
    $info = getimagesize( $nivel_base.'/imagenes/'.$carpeta.'/img_'.$random.'.'.$extension);
    if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
        return 'img_'.$random.'.'.$extension;
    }
    return false;
}

function comprobarImagenBase64($base64) {
    global $nivel_base;
    $exploded = explode(',', $base64, 2); // limit to 2 parts, i.e: find the first comma
    $encoded = $exploded[1]; // pick up the 2nd part
    $decoded = base64_decode($encoded);
    $img = imagecreatefromstring($decoded);
    if (!$img) {
        return false;
    }
    $random = randomString(30);
    mkdir($nivel_base.'/temp/base64images/'.$random, 0777, true);
    imagepng($img, $nivel_base.'/temp/base64images/'.$random.'/tmp.png');
    $info = getimagesize($nivel_base.'/temp/base64images/'.$random.'/tmp.png');
    unlink($nivel_base.'/temp/base64images/'.$random);
    if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
        return true;
    }
    return false;
}

if( !function_exists('boolval')) { // Función para convertir una variable en boolean (Nativa de PHP, no soportada en PHP 5.4 e inferiores)
  function boolval($var){
    return !! $var;
  }
}

if(!function_exists('array_to_xml')) {
  function array_to_xml( $data, &$xml_data) {
    foreach($data as $key => $value ) {
      if(is_numeric($key) ){
        $key = 'item-'.$key; // Correxión de los problemas de arrays numéricos...
      }
      if(is_array($value) ) {
        $subnode = $xml_data->addChild($key);
        array_to_xml($value, $subnode); // Se repite infinito en arrays dentro de arrays...
      } else {
        $xml_data->addChild("$key",htmlspecialchars("$value"));
      }
     }
  }
}

if(!function_exists('generarXML')) {
  function generarXML($data = [],$root = null) { // Genera un XML a partir de información dada en un ARRAY...
    $root = ($root != null) ? $root : '<?xml version="1.0"?><data></data>'; // Marcado inicial del XML (Default)
    $xml_data = new SimpleXMLElement($root);
    array_to_xml($data,$xml_data);
    return $xml_data->asXML();
  }
}

if(!function_exists('miles')) {
  function miles($numero) { // Formatea números a formato de miles (Con separador de punto)
    $numero = number_format(intval($numero),0,",",".");
    return $numero;
  }
}


  if(!function_exists('bytes')) {
    function bytes($bytes, $force_unit = NULL, $format = NULL, $si = TRUE){ // Tranforma bytes a su equivalente unidad con sufijo...
        $format = ($format === NULL) ? '%01.2f %s' : (string) $format;
        if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE) {
            $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            $mod   = 1024;
        }
        else {
            $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
            $mod   = 1000;
        }
        if (($power = array_search((string) $force_unit, $units)) === FALSE){
            $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
        }
        return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
    }
  }


if(!function_exists('validarRut')) {
  function validarRut($rut) { // Comprueba un RUT
    if (!preg_match("/^[0-9.]+[-]?+[0-9kK]{1}/", $rut)) {
            return false;
        }
        $rut = preg_replace('/[\.\-]/i', '', $rut);
        $dv = substr($rut, -1);
        $numero = substr($rut, 0, strlen($rut) - 1);
        $i = 2;
        $suma = 0;
        foreach (array_reverse(str_split($numero)) as $v) {
            if ($i == 8)
                $i = 2;
            $suma += $v * $i;
            ++$i;
        }
        $dvr = 11 - ($suma % 11);
        if ($dvr == 11)
            $dvr = 0;
        if ($dvr == 10)
            $dvr = 'K';
        if ($dvr == strtoupper($dv))
            return true;
        else
            return false;
  }
}

if(!function_exists('capitalizarCadena')) {
  function capitalizarCadena($string) { // Convierte una cadena a un formato capitalizado (Para cadenas en mayúsculas)
    $exclude = ['y','o','de','del','mi'];
    $words = explode(' ', $string);
    $newStr = [];
    foreach($words as $key => $word) {
        //$minus = strtolower($word); // Problemático con tildes y ñ...
        $minus = mb_convert_case($word, MB_CASE_LOWER, "UTF-8");
        if(in_array($minus, $exclude)) {
          $newStr[] = $minus;
        }else {
          $newStr[] = ucfirst($minus);
        }
    }
    $newString = implode(' ', $newStr);
    return $newString;
  }
}
if(!function_exists('caching')) {
  function caching($dev = true){ // Genera un string para agregar al final un versioning...
    if($dev == true) {
      return '?v='.date("YmdHis");
    }
    else {
      return null;
    }
  }
}

if(!function_exists('is_ssl')) {
  function is_ssl() { // Consulta si no se está usando un certificado de seguridad
      if ( isset($_SERVER['HTTPS']) ) {
          if ( 'on' == strtolower($_SERVER['HTTPS']) )
              return true;
          if ( '1' == $_SERVER['HTTPS'] )
              return true;
      } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
          return true;
      }
      return false;
  }
}


?>
