
<section id="login-form" class="shadow radius relative">
  <div class="login-inner">
  <a class="absolute login-credits ctext shadow hidden">M.</a>
  <form action="requests/login.php" method="POST">
    <div class="ctext mbot10">
        <div style="background:url(../uploads/avatars/sample.jpg) no-repeat" class="relative login-avatar rounded inline-block loaded bg-fix"></div>
    </div>
    <div class="mbot ctext fs24 welcome-login loaded"><?=$lang['login_welcome']?></div>
    <div class="mbot20 ctext sublime-text"><?=$lang['login_description']?></div>
    <div class="input-group mbot20">
      <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
      <input name="email" class="fwidth block" placeholder="<?=$lang['login_email_placeholder']?>" type="text" data-tipo="email" data-requerido="true">
    </div>
    <div class="input-group mbot20">
      <span class="input-group-addon"><i class="fa fa-key"></i></span>
      <input name="password" class="fwidth block" placeholder="<?=$lang['login_password_placeholder']?>" type="password" data-tipo="texto" data-requerido="true">
    </div>
    <input class="block button" type="submit" value="<?=$lang['login_button_label']?>">
    <div class="ctext mtop20">
        <a href="#" class="underline b toggle-password"><?=$lang['login_password_forgot']?></a>
    </div>
  </form>
  </div>
</section>


<section id="password-forget" class="whitebg shadow radius fixed">
  <div class="ctext mbot20 padlock">
    <i class="fas fa-envelope-open fa-4x"></i>
  </div>
  <div class="mbot10 b italic"><?=$lang['login_instructions_recovery']?></div>
  <form>
    <div class="input-group mbot20">
      <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
      <input name="email" class="fwidth block" placeholder="<?=$lang['login_email_placeholder']?>" type="text" data-tipo="email" data-requerido="true">
    </div>
    <input class="block fwidth mbot10 button" type="submit" value="<?=$lang['login_button_recovery_label']?>">
    <a class="button cancel fwidth toggle-password" href="#"><i class="fa fa-reply"></i>&nbsp;&nbsp;<?=$lang['login_back_button']?></a>
  </form>
</section>
