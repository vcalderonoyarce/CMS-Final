<script>
$(document).ready(function(){
  function resize404() {
    var height = $(window).height();
    $('#not-found').css('min-height',height);
  }
  resize404();
  $(window).resize(function(){
    resize404();
  });
});
</script>

<style>
  html,body {
    overflow: hidden;
  }
</style>

<div class="clearfix"></div>
<div id="not-found" class="whitefont relative">
  <div class="container relative">
    <div class="absolute fwidth perspective" id="not-found-content">
      <div class="fs48 mbot10 label lf">Oops!</div>
      <div class="fs18 italic lf">
        <?=$lang['not_found_description']?>...
      </div>
      <div class="mtop30">
        <a href="<?=$cms_path?>" class="special-button">Ir a la página principal</a>
      </div>
    </div>
  </div>
</div>
