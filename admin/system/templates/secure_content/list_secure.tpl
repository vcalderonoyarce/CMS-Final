<?php

$encrypt = new Encryption();
$tarjeta = new CreditCard();
$tarjeta_encriptada = $registros[$x][$campo['nombre_campo']];
$tarjeta_descifrada = $encrypt->decrypt($tarjeta_encriptada);
$tipo_tarjeta = $tarjeta->detect(preg_replace('/\s+/', '', $tarjeta_descifrada));
echo '<div title="'.$lang['credit_card_title'].'" class="help"><span title="'.$tipo_tarjeta.'" class="cc-icon d-inline-block radius valign cc-'.$tipo_tarjeta.'">&nbsp;</span>&nbsp;&nbsp;**** **** **** <span class="b">'.substr($tarjeta_descifrada, -4).'</span></div>';
