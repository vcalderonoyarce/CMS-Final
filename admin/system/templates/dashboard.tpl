<?php
$hora = date("H");
$say_hello = ($hora > 2) ? (($hora > 21) ? $lang['good_night'] : $lang['good_afternoon']) : $lang['good_morning'];
?>

<section id="home-content">
    <div class="row">
      <div class="col-12 col-md-12 col-sm-12 col-lg-6">
        <div class="shadow radius whitebg home-widget">
          <h1 class="mbot20 nomargin fs14 b"><?=$lang['hi']?>, <?=$user_data['nombre']?></h1>
        </div>
      </div>
    </div>
</section>
