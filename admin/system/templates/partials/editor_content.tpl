<div id="gradient"></div>
<?php
    $reg = $cms->db->runQuery('SELECT * FROM ? WHERE ? = ?',[$info_tabla['ajustes_tabla']['nombre_tabla'],$table_pk,$id]);
    $reg = $reg[0];
    $tabs = [];
    $eliminar = [];
    foreach($info_tabla['campos_tabla'] as $key => $campo) { // Almaceno todos los campos en la pestaña general
        $tabs[$lang['main_tab']][] = $campo;
    }
    if($info_tabla['tabs'] !== null) {
      ?>
      <div id="all-tabs" class="relative">
        <a class="inline-block absolute text-center radius" href="#" id="show-tabs">&nbsp;<i class="valign fas fa-bars"></i>&nbsp;</a>
        <div id="pending-tabs" class="absolute radius shadow"></div>
      <div id="tabs-cont">
        <div id="editor-tabs" class="relative">
          <a href="#" data-target="#tab-<?=url_permalink($lang['main_tab'])?>" class="relative inline-block editor-tab b active"><?=$lang['main_tab']?><div class="error absolute text-center"></div></a>
          <?php foreach($info_tabla['tabs'] as $tabname => $tab) {?>
            <a href="#" data-target="#tab-<?=url_permalink($tabname)?>" class="relative inline-block editor-tab b"><?=$tabname?><div class="error absolute text-center"></div></a>
          <?php
         } ?>
         <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php

      $tabs_externas = [];
      foreach($info_tabla['tabs'] as $tabname => $campos) {
        // Primero, busco el key...
        foreach($campos as $key => $campo) {
          $key_ = array_search($campo, array_column($tabs[$lang['main_tab']], 'nombre_campo'));
          if($key_ !== false) {
            $tabs_externas[$tabname][] = $tabs[$lang['main_tab']][$key_];
            $eliminar[] = $key_;
          }
        }
      }

      foreach($eliminar as $k) {
        unset($tabs[$lang['main_tab']][$k]);
      }
      $tabs = array_merge($tabs,$tabs_externas);
      unset($tabs_externas);
    }
  ?>

    <?php
    $tindex = 0;
     foreach($tabs as $tabname => $items) { ?>

      <div class="<?=($tindex == 0) ? 'active ' : null ?> tab-content relative radius shadow whitebg" id="tab-<?=url_permalink($tabname)?>">

          <div class="fs24 mbot10 d-none visible-print-block b"><?=capitalizarCadena($tabname)?>
            <div class="separator mtop10"></div>
          </div>
        <?php
          foreach($items as $key => $campo) {
              // Los campos cms_usuarios_id, fecha_creacion y fecha_modificacion, son campos internos...
              if($campo['oculto_editor'] != true && $campo['nombre_campo'] != 'cms_usuarios_id' && $campo['nombre_campo'] != 'fecha_creacion' && $campo['nombre_campo'] != 'fecha_modificacion') {
              ?>
              <div class="row row-editor content-item-type-<?=$campo['tipo_campo']?> content-item-<?=url_permalink($campo['nombre_campo'])?>">
                <div class="col-12 col-sm-12 col-md-12 col-lg-2 b">
                  <?php
                    if($campo['tipo_campo'] != 'primary_key') {
                      ?>
                      <?php
                      $campo_nombre =($campo['tipo_campo'] == 'foreign_key') ? $cms->ortografia(str_replace('_',' ',str_replace('_',' ',singularizarCadena($cms->nombreCampoFK($campo['nombre_campo'])))),$lang['lang_prefix']) : $cms->ortografia(str_replace('_',' ',$campo['nombre_campo']),$lang['lang_prefix']);
                      echo capitalizarCadena($campo_nombre);
                      echo ($campo['requerido']) ? '<span class="required-dot">*</span>' : null;
                      ?>
                      <?php
                    }
                  ?>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                  <?=strlen(trim($campo['comentario_campo'])) > 0 ? '<div class="field-tip italic mbot10 hidden-print">'.$campo['comentario_campo'].'</div>' : null?>
                  <?php

                  /* Pendiente:
                    - Videos
                    - Videos incrustados
                    - FK
                    - OBLIGATORIOS
                    - RUT
                    - Imagen
                    - CC
                    - PW
                    - Tags
                    - Enriquecidos
                  */
                  switch ($campo['tipo_campo']) {
                    case 'primary_key':
                    ?>
                      <input required="required" class="hidden" name="<?=$campo['nombre_campo']?>" value="<?=$reg[$campo['nombre_campo']]?>">
                      <?php
                      break;
                    case 'checkbox':
                    ?>
                      <div class="custom-checkbox">
                        <input <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($reg[$campo['nombre_campo']] == '1') ? 'checked="checked"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> id="<?=$campo['nombre_campo']?>" type="checkbox" name="<?=$campo['nombre_campo']?>">
                        <label <?=($campo['solo_lectura']) ? 'onclick="return false;"' : null ?> for="<?=$campo['nombre_campo']?>"></label>
                      </div>
                      <?php
                      break;
                    case 'imagen':
                    ?>
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                        <?php
                          $imgsize = ($reg[$campo['nombre_campo']] != null) ? file_exists('../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']]) ? ' - '.bytes(filesize('../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']])) : null : null;
                        ?>
                        <div class="relative <?=($reg[$campo['nombre_campo']] != null) ? 'loaded-image' : 'not-image res-layer radius' ?> image-pl mbot10" id="pl-<?=$campo['nombre_campo']?>" data-rel=".5" data-rel-tablet=".5" data-rel-mobile=".5">
                          <?php
                            if($reg[$campo['nombre_campo']] != null && $campo['solo_lectura'] == false) {
                              echo '<a title="'.$lang['remove_current_img'].'" href="#" class="incolor absolute delete-image radius shadow block text-center" data-table="'.$info_tabla['ajustes_tabla']['id_tabla'].'" data-id="'.$reg[$table_pk].'" data-column="'.$campo['nombre_campo'].'">&nbsp;<i class="fas fa-times valign"></i>&nbsp;</a>';
                            }
                          ?>

                          <?=($reg[$campo['nombre_campo']] != null) ? '<div title="'.$lang['current_image'].' ('.$reg[$campo['nombre_campo']].$imgsize.')" class="inline-block image-bg"><img data-featherlight="#pl-'.$campo['nombre_campo'].'" class="disable-on-featherlight block help responsive radius" src="../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']].'"></div>' : null ?>
                          <div class="show image-layer b text-center">
                            <i class="fas fa-3x fa-cloud-upload-alt"></i>
                            <br>
                            <?=$lang['accepted-images']?><br/>
                            .PNG, .JPEG, .JPG, .GIF</div>
                        </div>
                        <?php if($campo['solo_lectura'] == false):?>
                        <div class="custom-image-upload relative">
                          <input class="absolute" <?=$campo['requerido'] == true ? 'required="required"' : null?> data-placeholder="#pl-<?=$campo['nombre_campo']?>" accept="image/*" type="file" name="<?=$campo['nombre_campo']?>" id="img-<?=$campo['nombre_campo']?>">
                          <label class="button fwidth" <?=($campo['solo_lectura']) ? 'onclick="return false;"' : null ?> for="img-<?=$campo['nombre_campo']?>">
                            <i class="fas fa-upload"></i> &nbsp;&nbsp;<?=$lang['upload_image']?>
                          </label>
                        </div>
                        <?php endif;?>
                      </div>
                    </div>
                      <?php
                      break;

                      case 'tags':
                      ?>
                        <input value="<?=$reg[$campo['nombre_campo']]?>" type="text" <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> <?=$campo['requerido'] == true ? 'required="required"' : null?> id="<?=$campo['nombre_campo']?>" <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="tag-input rich fwidth block">
                      <?php
                      break;
                      case 'enriquecido':
                      ?>
                        <textarea <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> <?=$campo['requerido'] == true ? 'required="required"' : null?> id="<?=$campo['nombre_campo']?>" <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="rich fwidth block"><?=$reg[$campo['nombre_campo']]?></textarea>
                      <?php
                      break;
                      case 'video':
                      ?>
                      <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-5">
                          <?php
                            $imgsize = ($reg[$campo['nombre_campo']] != null) ? file_exists('../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']]) ? ' - '.bytes(filesize('../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']])) : null : null;
                          ?>
                          <div class="relative <?=($reg[$campo['nombre_campo']] != null) ? 'loaded-image' : 'not-image res-layer radius' ?> image-pl mbot10" id="pl-<?=$campo['nombre_campo']?>" data-rel=".5" data-rel-tablet=".5" data-rel-mobile=".5">
                            <?php
                              if($reg[$campo['nombre_campo']] != null && $campo['solo_lectura'] == false) {
                                echo '<a title="'.$lang['remove_current_video'].'" href="#" class="incolor absolute delete-image radius shadow block text-center" data-table="'.$info_tabla['ajustes_tabla']['id_tabla'].'" data-id="'.$reg[$table_pk].'" data-column="'.$campo['nombre_campo'].'">&nbsp;<i class="fas fa-times valign"></i>&nbsp;</a>';
                              }
                            ?>

                            <?=($reg[$campo['nombre_campo']] != null) ? '<div title="'.$lang['current_image'].' ('.$reg[$campo['nombre_campo']].$imgsize.')" class="inline-block image-bg"><img data-featherlight="#pl-'.$campo['nombre_campo'].'" class="disable-on-featherlight block help responsive radius" src="../uploads/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.$reg[$campo['nombre_campo']].'"></div>' : null ?>
                            <div class="show image-layer b text-center">
                              <i class="fas fa-3x fa-cloud-upload-alt"></i>
                              <br>
                              <?=$lang['accepted_videos']?><br/>
                              .MP4 .WMV .WEBM</div>
                          </div>
                          <?php if($campo['solo_lectura'] == false):?>
                          <div class="custom-image-upload relative">
                            <input class="absolute" <?=$campo['requerido'] == true ? 'required="required"' : null?> data-placeholder="#pl-<?=$campo['nombre_campo']?>" accept="video/*" type="file" name="<?=$campo['nombre_campo']?>" id="video-<?=$campo['nombre_campo']?>">
                            <label class="button fwidth" <?=($campo['solo_lectura']) ? 'onclick="return false;"' : null ?> for="video-<?=$campo['nombre_campo']?>">
                              <i class="fas fa-upload"></i> &nbsp;&nbsp;<?=$lang['upload_video']?>
                            </label>
                          </div>
                          <?php endif;?>
                        </div>
                      </div>
                      <?php
                      break;

                      case 'foreign_key':
                        $tabla_fk = $cms->nombreCampoFK($campo['nombre_campo'],false);
                        $datos_fk = $cms->datosForaneos($tabla_fk);
                        if(is_array($datos_fk)) {
                          ?>
                          <div class="custom-selecs fwidth">
                            <select <?=$campo['requerido'] == true ? 'required="required"' : null?> class="custom-select" <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>"><option value=""><?=$lang['choose_an_option']?></option>
                              <option><?=$lang['select_option']?></option>
                            <?php
                            foreach($datos_fk as $key => $item) {
                            ?>
                             <option <?=$item['fkey'] == $reg[$campo['nombre_campo']] ? 'selected="selected"' : null?> value="<?=$item['fkey']?>"><?=$item['nombre']?></option>
                            <?php
                            }
                            ?>
                            </select>
                          </div>
                          <?php
                        }
                      ?>

                      <?php
                      break;
                      case 'fecha':
                      ?>
                        <div class="relative input-container date-cont">
                          <input pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> id="<?=$campo['nombre_campo']?>" <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth date-picker" type="text" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                        </div>
                      <?php
                      break;
                      case 'hora':
                        ?>
                        <input pattern="([0-1]{1}[0-9]{1}|20|21|22|23):[0-5]{1}[0-9]{1}" <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> id="<?=$campo['nombre_campo']?>" <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth clock-picker" type="text" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                        <?php
                      break;
                      case 'numerico':
                        if(isset($campo['ajustes_adicionales']['formato'])) { // Si existen ajustes adicionales
                          switch ($campo['ajustes_adicionales']['formato']) {
                            case 'select':
                              // code...
                              if(!isset($campo['ajustes_adicionales']['minimo']) || !isset($campo['ajustes_adicionales']['maximo'])) {
                                echo '<div class="invalid-field radius">'.$lang['invalid_field_config'].'</div>';
                                guardarLog('El campo \''.$campo['nombre_campo'].'\' en la tabla \''.$info_tabla['ajustes_tabla']['nombre_tabla'].'\' requiere tener definidos un mínimo y un máximo',true);
                              }
                              else{
                                $salto = isset($campo['ajustes_adicionales']['salto']) ? intval($campo['ajustes_adicionales']['salto']) : 1;
                                $min = intval($campo['ajustes_adicionales']['minimo']);
                                $max = intval($campo['ajustes_adicionales']['maximo']);
                                echo '<select '.($campo['requerido'] == true ? 'required' : null).' '.($campo['solo_lectura'] == true ? 'readonly' : null).' class="custom-select" name="'.$campo['nombre_campo'].'"><option value="">'.$lang['choose_an_option'].'</option>';
                                for($x = $min; $x <= $max; $x+=$salto) {
                                   echo '<option value="'.$x.'" '.($reg[$campo['nombre_campo']] == $x ? 'selected' : null).'>'.$x.'</option>';
                                }
                                echo '</select>';
                              }

                            break;

                            default:
                              ?>
                              <input <?=isset($campo['ajustes_adicionales']['minimo']) ? 'min="'.$campo['ajustes_adicionales']['minimo'].'"' : null?> <?=isset($campo['ajustes_adicionales']['maximo']) ? 'max="'.$campo['ajustes_adicionales']['maximo'].'"' : null?> onkeydown="javascript: return (event.keyCode == 107 || event.keyCode == 109 || event.keyCode == 69) ? false : true;" <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth" type="number" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                              <?php
                              break;
                          }
                        }
                        else {
                          ?>
                          <input <?=isset($campo['ajustes_adicionales']['minimo']) ? 'min="'.$campo['ajustes_adicionales']['minimo'].'"' : null?> <?=isset($campo['ajustes_adicionales']['maximo']) ? 'max="'.$campo['ajustes_adicionales']['maximo'].'"' : null?> onkeydown="javascript:<?=($campo['longitud_maxima'] != null) ? 'if(this.value.length=='.$campo['longitud_maxima'].' && event.keyCode != 8) return false;' : null?> return (event.keyCode == 107 || event.keyCode == 109 || event.keyCode == 69) ? false : true;" <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth" type="number" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                          <?php
                        }
                      break;
                      case 'rut':
                      ?>
                        <input <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> id="<?=$campo['nombre_campo']?>" <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth rut-check" type="text" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                      <?php
                      break;
                    default:
                      // code...
                      ?>
                      <input <?=($campo['longitud_maxima'] != null) ? 'maxlength="'.$campo['longitud_maxima'].'"' : null?> <?=$campo['requerido'] == true ? 'required="required"' : null?> <?=($campo['solo_lectura']) ? 'readonly="readonly"' : null ?> name="<?=$campo['nombre_campo']?>" class="fwidth" type="text" value="<?=htmlentities($reg[$campo['nombre_campo']])?>">
                      <?php
                      break;
                  }

                  ?>
                </div>
              </div>
              <?php
              }
          }
        ?>
      </div>
    <?php
    $tindex++;
   } ?>
