<?php
  // d($permisos_tabla);
  //d($info_tabla);

?>
<div id="cms-header">
    <div class="row">
      <div class="header-name col-12 col-sm-12 col-md-12 col-lg-4">
        <div class="lf">
          <?=capitalizarCadena($info_tabla['ajustes_tabla']['nombre_plural'])?>
        </div>
      </div>
      <div class="right-actions col-12 col-sm-12 col-md-12 col-lg-8 text-left text-sm-left text-md-left text-lg-right hidden-print">
        <?=($permisos_tabla['puede_agregar']) ? '<a title="'.$lang['create_title'].' '.$info_tabla['ajustes_tabla']['nombre_singular'].'" class="button" href="javascript:alert(\'pendiente\')"><span class="bolder fs18 vbalign">+</span> <span class="d-none d-sm-none d-md-inline-block d-lg-inline-block">'.$lang['add_button'].' '.$info_tabla['ajustes_tabla']['nombre_singular'].'</span></a>' : NULL?>
        <?=($permisos_tabla['puede_descargar']) ? '&nbsp;&nbsp;<a class="button icon-button green-button" title="'.$lang['download_csv_title'].'" href="download.php?id='.$info_tabla['ajustes_tabla']['id_tabla'].'"><img class="icon-img-s" src="front/img/excel.svg">&nbsp; .csv</a>' : null?>
        <?=($permisos_tabla['puede_ver_informe']) ? '&nbsp;&nbsp;<a class="button icon-button green-button" title="'.$lang['view_report_title'].'" href="download.php?id='.$info_tabla['ajustes_tabla']['id_tabla'].'"><i class="fas fa-chart-pie"></i><span class="d-none d-sm-none d-md-inline-block d-lg-inline-block">&nbsp;&nbsp;'.$lang['view_report'].'</span></a>' : null?>
      </div>
    </div>
</div>
<div class="mbot15" id="gradient"></div>
    <div id="ajax-search" class="text-right mbot15">
      <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-8"></div>
          <div class="col-12 col-sm-12 col-md-12 col-lg-4 fright">
            <div class="relative" id="search-placer"><input class="fwidth" placeholder="<?=$lang['search_pl']?> <?=$info_tabla['ajustes_tabla']['nombre_singular']?>" type="text" id="ajax-search-input" data-table-id="<?=$info_tabla['ajustes_tabla']['nombre_tabla']?>"></div>
          </div>
      </div>
    </div>
  <div id="list-holder" class="whitebg shadow radius">
  <div id="list-content">
  <table id="table-list">
    <thead>
    <?php

    // Preparo la consulta para traer los inputs....
    $db = new Database();
    $table_pk = $cms->getPk($info_tabla['ajustes_tabla']['nombre_tabla']);
    $db->prepareQuery('SELECT ? FROM ?',[$table_pk,$info_tabla['ajustes_tabla']['nombre_tabla']]);
    if($info_tabla['ajustes_tabla']['por_usuario'] == true) {
      // Agregar columna (Si ésta no existe)...
      // Pendiente
      $db->addColumnInTable($info_tabla['ajustes_tabla']['nombre_tabla'],'cms_usuarios_id');
      $db->addWhere('cms_usuarios_id = '.$_SESSION['cms_user']);
    }


    foreach($info_tabla['campos_tabla'] as $key => $campo) {
      if($campo['oculto_lista'] == false) {
        if($campo['nombre_campo'] != $table_pk) { // Lo trae por defecto el manejador
            $db->addColumn(''.$campo['nombre_campo']);
        }
      ?>
      <th class="text-center b th-<?=url_permalink($campo['nombre_campo'])?>">
        <?php
          if($campo['nombre_campo'] == 'cms_usuarios_id') {
           ?>
           <?=$lang['user_label']?>
           <?php
          }
          else {
            ?>
            <?=($campo['tipo_campo'] == 'foreign_key') ? $cms->ortografia(str_replace('_',' ',capitalizarCadena(str_replace('_',' ',capitalizarCadena(singularizarCadena($cms->nombreCampoFK($campo['nombre_campo'])))))),$lang['lang_prefix']) : $cms->ortografia(str_replace('_',' ',capitalizarCadena($campo['nombre_campo'])),$lang['lang_prefix']) ?>
            <?php
          }
          $sort = isset($_GET['sort']) && strlen(trim($_GET['sort'])) > 0 ? htmlentities($_GET['sort'])  :null;
        ?>

      </th>
      <?php
      }
    }


    ?>
    <?php if($permisos_tabla['puede_modificar'] == true || $permisos_tabla['puede_eliminar'] == true) :?>
      <th class="editor-th"></th>
    <?php endif;?>
    </thead>

    <tbody>
      <?php
      // Genero paginación
      $pagina = (isset($_GET['pagina']) && strlen(trim($_GET['pagina'])) != 0) ? intval($_GET['pagina']) : 1;
      $ipp = (isset($_GET['ipp']) && strlen(trim($_GET['ipp'])) != 0) ? intval($_GET['ipp']) : 10;
      $total_paginacion = $db->runQuery('SELECT count(*) as cant FROM ? ',[$info_tabla['ajustes_tabla']['nombre_tabla']]);
      $total_registros = is_array($total_paginacion) ? intval($total_paginacion[0]['cant']) : 0;
      $paginas = ceil($total_registros / $ipp);
      $offset = ($pagina - 1) * $ipp;
      $db->endQuery("ORDER BY $table_pk DESC LIMIT $offset, $ipp");
      $registros = $db->execQuery();
      if(is_array($registros)) {
        for($x = 0;$x < count($registros); $x++) {
          echo '<tr>';
          foreach($info_tabla['campos_tabla'] as $key => $campo) {
            if($campo['oculto_lista'] == false) {
                ?>
                <td class="td-type-<?=$campo['tipo_campo']?> td-<?=url_permalink($campo['nombre_campo'])?>">
                  <?php
                    switch ($campo['tipo_campo']) {
                      case 'checkbox':
                        ?>
                        <div class="text-center">
                          <div class="custom-checkbox relative inline-block">
                            <input class="dinamic-checkbox" data-tablename="<?=$info_tabla['ajustes_tabla']['nombre_tabla']?>" data-id="<?=$registros[$x][$table_pk]?>" id="<?=$campo['nombre_campo']?>-<?=$x?>" <?=($registros[$x][$campo['nombre_campo']] == '1') ? 'checked="checked"' : NULL?> type="checkbox">
                            <label for="<?=$campo['nombre_campo']?>-<?=$x?>"></label>
                          </div>
                        </div>
                        <?php
                        break;
                      case 'imagen':
                        ?>
                        <?=(strlen(trim($registros[$x][$campo['nombre_campo']])) == 0) ? '<div class="text-center">--</div>' : '<a style="background:url('.$nivel_base.'../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.trim($registros[$x][$campo['nombre_campo']]).') no-repeat center" data-featherlight="image" href="'.$nivel_base.'../uploads/images/'.url_permalink($info_tabla['ajustes_tabla']['nombre_plural']).'/'.trim($registros[$x][$campo['nombre_campo']]).'" class="block pointer load-img radius bg-size res-layer" data-rel="1" data-rel-tablet="1" data-rel-mobile="1"></a>' ?>
                        <?php
                      break;
                      case 'video':
                        ?>
                        <?=(strlen(trim($registros[$x][$campo['nombre_campo']])) == 0) ? '<div class="text-center">--</div>' : '
                        <a title="'.$lang['click_to_play_video'].'" href="../uploads/'.$info_tabla['ajustes_tabla']['nombre_plural'].'/'.trim($registros[$x][$campo['nombre_campo']]).caching().'" data-featherlight="iframe" class="video-thumbnail radius pointer relative ovhidden res-layer block" data-rel="1" data-rel-tablet="1" data-rel-mobile="1" id="v-'.$campo['nombre_campo'].$x.'">
                          <div class="image-layer">
                            <video width="400" preload="metadata">
                              <source src="../uploads/'.$info_tabla['ajustes_tabla']['nombre_plural'].'/'.trim($registros[$x][$campo['nombre_campo']]).caching().'#t=0.5" type="video/mp4">
                              '.$lang['video_tag_html_support'].'
                            </video>
                          </div>
                        </a>' ?>
                        <?php
                      break;
                      case 'password':
                      if(strlen(trim($registros[$x][$campo['nombre_campo']])) > 0) {
                      ?>
                      <div class="help" title="<?=$lang['safe_passwords_layer']?>">
                        <img class="icon-img" src="front/img/shield.svg">
                      <i><?=$lang['protected_content']?></i>
                      </div>
                      <?php
                      }
                      else {
                        ?>
                        <div class="text-center">--</div>
                        <?php
                      }

                      break;
                        case 'fecha':
                        if(preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$registros[$x][$campo['nombre_campo']]) == true) {
                          $date = new Datetime($registros[$x][$campo['nombre_campo']].' 00:00:00');
                          $m = intval($date->format('m'))-1;
                          $fecha = $date->format($lang['date_format']);
                          $fecha = replace_between($fecha, '{', '}', $lang['months'][$m]);
                          $fecha = removeChars($fecha,['{','}']);
                          echo $fecha;
                          ?>
                          <?php
                        }
                        else {
                          ?>
                          <div class="text-center">--</div>
                          <?php
                        }

                        break;
                        case 'fecha_completa':
                          try {
                              if(trim(strlen($registros[$x][$campo['nombre_campo']])) == 0) {
                                throw new \Exception(null, 1);
                              }
                              $date = new DateTime($registros[$x][$campo['nombre_campo']]);
                              $m = intval($date->format('m'))-1;
                              $fecha = $date->format($lang['full_date_format']);
                              $fecha = replace_between($fecha, '{', '}', $lang['months'][$m]);
                              $fecha = removeChars($fecha,['{','}']);
                              echo $fecha;
                          } catch (\Exception $e) {
                            echo '<div class="text-center">--</div>';
                          }

                        break;
                      case 'archivo':
                          $ext = extension_archivo($registros[$x][$campo['nombre_campo']]);
                        ?>
                        <a target="blank" class="file-table radius relative block" href="<?=$nivel_base?>../uploads/<?=url_permalink($info_tabla['ajustes_tabla']['nombre_plural'])?>/<?=$registros[$x][$campo['nombre_campo']]?>">
                          <div class="file-icon inline-block b relative file-<?=$ext?>">
                            <?=$ext?>
                          </div>
                          <br/>
                          <div class="b file-name inline-block text-center"><?=$registros[$x][$campo['nombre_campo']]?></div>
                        </a>
                        <?php
                        break;
                      case 'foreign_key':
                        ?>
                        <?php
                        $dato_pk = strlen(trim($registros[$x][$campo['nombre_campo']])) > 0 ? $cms->datoForaneo($registros[$x][$campo['nombre_campo']],$campo['nombre_campo']) : null;
                        if($dato_pk === false) {
                          guardarLog('Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe',true);
                          echo '<i title="'.$lang['not_defined_pk'].'" class="help fas fa-exclamation-triangle"></i>';
                        }
                        else {
                          if($dato_pk != null) {
                              echo $dato_pk;
                          }
                          else {
                            echo '<div class="text-center">--</div>';
                          }
                        }

                      break;
                      case 'tags':
                      if(strlen(trim($registros[$x][$campo['nombre_campo']])) > 0) {
                        $tags = explode(',',$registros[$x][$campo['nombre_campo']]);
                        foreach($tags as $kt => $tag) {
                          if(trim($tag) != '') {
                        ?>
                          <span class="dashboard-tag help" title="<?=trim($tag)?>"><?=trim($tag)?></span><?=$kt+1 != count($tags) ? ',' : ''?>
                        <?php
                          }
                        }
                      }
                      else {
                        echo '<div class="text-center">--</div>';
                      }
                      break;
                      case 'tarjeta':
                        if(strlen(trim($registros[$x][$campo['nombre_campo']])) > 0) {
                          // Sistema de muestra de tarjetas de crédito, el uso de información de terceros corresponde a un delito,
                          // no se permite bajo ninguna circunstancia utlizar la información almacenada para fines personales.
                          // El código se encuentra encriptado mediante una clave de acceso la cual únicamente el creador conoce,
                          // sin embargo, eso no hace invulnerable el sistema.
                          // https://www.inf.utfsm.cl/~lhevia/asignaturas/infoysoc/topicos/Etica/3_normas_codigos_y_org_internac.pdf
                          require($nivel_base.'system/templates/secure_content/list_secure.tpl');

                        }
                        else {
                          echo '<div class="text-center">--</div>';
                        }

                      break;
                      case 'rut':
                        if(strlen(trim($registros[$x][$campo['nombre_campo']])) > 0) {
                          if(!isset($validador)){
                              $validador = new Validador();
                          }
                          $r_vclass = ($validador->validarRut($registros[$x][$campo['nombre_campo']]) == false) ? 'invalid' : null ;
                          ?>
                          <div <?=($r_vclass == 'invalid') ? 'title="'.$lang['invalid_chilean_rut'].'"' : null?> class="rut <?=$r_vclass?>"><?=$registros[$x][$campo['nombre_campo']]?></div>
                          <?php
                        }
                        else {
                          echo '<div class="text-center">--</div>';
                        }

                      break;
                      default:
                        if(strlen(trim($registros[$x][$campo['nombre_campo']])) == 0) {
                          echo '<div class="text-center">--</div>';
                        }
                        else {
                        ?>
                        <?=(strlen(strip_tags($registros[$x][$campo['nombre_campo']])) > 50) ? substr(strip_tags($registros[$x][$campo['nombre_campo']]),0,49).'...' : strip_tags($registros[$x][$campo['nombre_campo']]) ?>
                        <?php
                        }
                        break;
                    }
                  ?>

                </td>
                <?php
              }
            }
            // Permisos...
            ?>
            <?php if($permisos_tabla['puede_modificar'] == true || $permisos_tabla['puede_eliminar'] == true) :?>
            <td class="td-actions text-center">

              <div class="dropdown <?=($x < count($registros) - 2) ? null : 'dropup' ?>">
                <a href="#" id="menu-<?=$campo['nombre_campo'].$x?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img class="icon-img incolor" src="front/img/cog.svg">
                </a>
              <div class="dropdown-menu actions-dropdown" aria-labelledby="menu-<?=$campo['nombre_campo'].$x?>">
                <?=($permisos_tabla['puede_modificar'] == true)? '<a title="'.$lang['edit_title'].'" class="block b" href="edit/'.$info_tabla['ajustes_tabla']['id_tabla'].'/'.$registros[$x][$table_pk].'">'.$lang['edit_title'].'</a>' : ''?>
                <?=($permisos_tabla['puede_eliminar'] == true)? '<a title="'.$lang['delete_title'].'" class="block b" data-id="'.$registros[$x][$table_pk].'" data-tablename="'.$info_tabla['ajustes_tabla']['id_tabla'].'" href="#">'.$lang['delete_title'].'</a>' : ''?>
                <?=($permisos_tabla['puede_imprimir'] == true)? '<a title="'.$lang['print_title'].'" class="block b" data-id="'.$registros[$x][$table_pk].'" data-tablename="'.$info_tabla['ajustes_tabla']['id_tabla'].'" href="#">'.$lang['print_title'].'</a>' : ''?>
              </div>
            </div>
            </td>
            <?php
            endif;
            echo '</tr>';
        }
      }
      else {
        ?>
      <tr>
        <td>
          <div class="hidden">
            <style>#table-list {display:none}</style>
            <div id="no-regs" class="moveto ctext fs18" data-target="#translate-to"><?=$lang['not_reg_yet']?> <?=$info_tabla['ajustes_tabla']['nombre_plural']?></div>
          </div>
        </td>
      </tr>
        <?php
      }

      ?>

    </tbody>
  </table>
  <div id="translate-to"></div>
  </div>
  </div>

<?php
require($nivel_base.'system/classes/paginacion.class/paginacion.class.php');
$paginacion = new Paginacion();
$paginacion = $paginacion->renderPaginacion($paginas,$pagina,$ipp,'content/'.$info_tabla['ajustes_tabla']['id_tabla'].'/'.url_permalink($info_tabla['ajustes_tabla']['nombre_tabla']));
?>

  <div class="list-pagination bottom mbot15">
    <?=$paginacion?>
    <div class="fright">
      <form class="inline-block" id="changeIpp" action="<?='content/'.$info_tabla['ajustes_tabla']['id_tabla'].'/'.url_permalink($info_tabla['ajustes_tabla']['nombre_tabla'])?>">
        <!-- <input type="hidden" name="<?=$pagina?>" value="<?=$ipp?>"> -->
        <select class="custom-select" name="ipp" onchange="$('#changeIpp').submit();">
          <option <?=$ipp == 10 ? 'selected' : null ?> value="10">10</option>
          <option <?=$ipp == 20 ? 'selected' : null ?> value="20">20</option>
          <option <?=$ipp == 50 ? 'selected' : null ?> value="50">50</option>
          <option <?=$ipp == 100 ? 'selected' : null ?> value="100">100</option>
          <option <?=$ipp == 1000 ? 'selected' : null ?> value="1000">1000</option>
        </select>
      </form>
      <div class="fright ipp-label fs12 b">&nbsp;&nbsp;<?=capitalizarCadena($info_tabla['ajustes_tabla']['nombre_plural'])?> <?=$lang['ipp_label']?></div>
    </div>
    <div class="clearfix"></div>
  </div>
