<?php
  $cms = new CMS();
  $sidebar_links = $cms->tablasMenus($_SESSION['cms_user']);
?>
<div id="cms-menu" class="hidden-print text-center relative">
  <div id="sidebar-links">
    <?=$sidebar_links?>
  </div>
</div>
