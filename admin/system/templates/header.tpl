<?php
$notificaciones = new Notificaciones();
$lista_notificaciones = $notificaciones->cargarNotificaciones($_SESSION['cms_user']);
$cuenta_notificaciones = $notificaciones->cargarNotificaciones($_SESSION['cms_user'],'pendientes');
$cuenta_notificaciones = count($cuenta_notificaciones);
$usuario = new Usuario();
$perfil_nombre = $usuario->nombrePerfil($_SESSION['cms_user']);

?>


<header id="site-header" class="fixed">
  <div id="logo-menu" class="fleft text-center">
    <a href="../admin"><img class="responsive responsive_h" id="site-logo" src="front/img/cmslogo.png<?=caching()?>" alt="Logo"></a>
    <!-- <a class="whitefont b" href="../admin">LOGO</a> -->
  </div>
  <div id="header-content" class="fright">
      <div class="col-12 text-right hidden-print">
        <div class="relative inline-block">
          <div class="toggle-notifications n-icon header-link incolor b inline-block pointer relative text-center">
            <img class="icon-img" src="front/img/bell.svg">
            <?=($cuenta_notificaciones > 0) ? '<span class="notif-balloon ctext absolute">'.$cuenta_notificaciones.'</span>' : ''?>

            <div id="notifications" class="notifications absolute whitebg shadow radius">
              <div class="loading-notifications text-center">
                  <i class="fas fa-spinner fa-pulse valign"></i> &nbsp;<?=$lang['wait_loading_not']?>
            </div>
            </div>
          </div>
          <div class="user-holder header-link incolor b inline-block pointer relative">
            <div class="user-menu text-left absolute radius shadow whitebg">
              <a class="block user-link radius acc-sett-go" href="#"><?=$lang['user_edit_link']?></a>
              <a class="block user-link radius" href="#"><?=$lang['user_cms_edit']?></a>
              <a class="block user-link radius" href="#"><?=$lang['user_support']?></a>
              <a class="block user-link radius" href="logout"><?=$lang['user_sign_out']?></a>
            </div>
            <div class="inline-block rounded valign havatar relative">
              <img src="<?=$user_data['avatar']?>" class="toggle-header-menu no-select radius valign header-avatar">
            </div>
            <div class="inline-block toggle-header-menu no-select">&nbsp;&nbsp;<?=$user_data['nombre']?> <span class="hidden-md-down"><?=$user_data['apellido']?></span>&nbsp;&nbsp;<i class="fa fa-chevron-down"></i></div>
          </div>
        </div>
      </div>
  </div>


</header>

<section id="site-content">
