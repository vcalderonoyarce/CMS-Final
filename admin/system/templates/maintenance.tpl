<!DOCTYPE html>
<html class="whitebg">
  <head>
    <meta charset="utf-8">
    <title><?=$lang['maintenance_title']?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <base href="<?php echo $cms_path; ?>" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" href="<?=$nivel_base?>vendors/font-awesome/css/fontawesome-all.min.css<?=caching()?>">
    <link href="<?=$nivel_base?>vendors/tether/dist/css/tether.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/bootstrap/css/bootstrap.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/default.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/styles.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/tablet.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/mobile.css<?=caching()?>" rel="stylesheet">
  </head>
  <body class="whitebg">

    <div id="maintenance">
      <div class="container text-center">
        <img class="inline-block responsive" src="front/img/maintenance.gif">
        <div class="mtop20 mbot10 b fs24"><?=$lang['maintenance_title']?>...</div>
        <div class="b"><?=$lang['maintenance_detail']?>.</div>
      </div>
    </div>

  </body>
</html>
