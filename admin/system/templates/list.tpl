<?php

  $cms = new CMS();
  $table_id = (isset($_GET['table_id']) && strlen(trim($_GET['table_id'])) > 0) ? htmlentities($_GET['table_id']) : null;

?>

<div id="cms-table-regs">
  <?php

  try {
    if($table_id != null) {
      $table_is_init = $cms->tableisInit($table_id);
      if($table_is_init == false) {
        guardarLog('Se intentó ingresar a una tabla que no ha sido inicializada ('.$table_id.' no existe en el sistema), por el usuario ID: '.$_SESSION['cms_user'],true);
        throw new \Exception($lang['table_not_initialized'], 1);
      }
      else {
        $table_exists = $cms->tableExists($table_id);
        if($table_exists == false) {
          $nombre_tabla = $cms->nombreTabla($table_id);
          guardarLog('La tabla "'.$nombre_tabla.'" no existe en la base de datos',true);
          throw new \Exception($lang['table_not_created'], 1);
        }
        else {
          // Permisos...
          $permisos_tabla = $cms->tienePermisosTabla($_SESSION['cms_user'],$table_id);

          if($permisos_tabla['puede_visualizar'] == true) {
            $info_tabla = $cms->ajustesTabla($table_id);
            if(is_array($info_tabla)) {

              if($info_tabla['ajustes_tabla']['registro_unico'] == true) {
                // Cargar editor inmediatamente

              }
              else {

                $renderList = new Plantilla($nivel_base.'system/templates/partials/dashboard_list.tpl');
                $renderList->set('info_tabla',$info_tabla);
                $renderList->set('permisos_tabla',$permisos_tabla);
                $renderList->set('nivel_base',$nivel_base);
                $renderList->set('lang',$lang);
                $renderList->set('cms',$cms);
                echo $renderList->render();

              }
              ?>



              <?php
            }
            else {
              guardarLog('Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)',true);
              throw new \Exception($lang['error_table_settings'], 1);
            }
          }
          else {
            throw new \Exception($lang['no_permissions_to_view'], 1);
          }
        }
      }

    }
    else {
      throw new \Exception($lang['no_table_selected'], 1);
    }

  } catch (\Exception $e) {
    ?>
        <div id="generic-section" class="message radius">
          <div class="fs18 mbot10"><?=$e->getMessage()?></div>
          <a href="../admin" class="button"><i class="fas fa-reply"></i>&nbsp;&nbsp;<?=$lang['back_home_text']?></a>
        </div>
    <?php
  }
  ?>
</div>
