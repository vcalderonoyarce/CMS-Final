<?php
  $cms = new CMS();
  $table_id = isset($_GET['table_id']) && strlen(trim($_GET['table_id'])) > 0 ? htmlentities($_GET['table_id']) : null;
  $id = isset($_GET['id']) && strlen(trim($_GET['id'])) > 0 ? htmlentities($_GET['id']) : null;
  $new = (isset($_GET['new'])) ? true : false;
  // var_dump($id_tabla);
?>

<div id="editor">
    <?php try {
      if($table_id != null) {
        $table_is_init = $cms->tableisInit($table_id);
        if($table_is_init === false) {
          guardarLog('Se intentó ingresar a una tabla que no ha sido inicializada ('.$table_id.' no existe en el sistema), por el usuario ID: '.$_SESSION['cms_user'],true);
          throw new \Exception($lang['table_not_initialized'], 1);
        }
        else {
          $table_exists = $cms->tableExists($table_id);
          if($table_exists == false) {
            $nombre_tabla = $cms->nombreTabla($table_id);
            guardarLog('La tabla "'.$nombre_tabla.'" no existe en la base de datos',true);
            throw new \Exception($lang['table_not_created'], 1);
          }
          else {
            $permisos_tabla = $cms->tienePermisosTabla($_SESSION['cms_user'],$table_id);
            if($permisos_tabla['puede_modificar'] == true) {
              $info_tabla = $cms->ajustesTabla($table_id);
              if(is_array($info_tabla)) {
                ?>
                <form action="requests/modify_registry.php" method="post" id="editor-form">
                <div id="cms-header">
                  <div class="container">
                    <div class="row">
                      <div class="header-name col-12 col-sm-12 col-md-12 col-lg-4">
                        <div class="lf upp whitefont">
                          <?=($new == true) ? $lang['new_label'] : $lang['edit_label'] ?>  <?=capitalizarCadena($info_tabla['ajustes_tabla']['nombre_singular'])?>
                        </div>
                      </div>
                      <div class="right-actions col-12 col-sm-12 col-md-12 col-lg-8 text-left text-sm-left text-md-left text-lg-right hidden-print">
                        <button type="submit" id="ed-save"><i class="fas fa-save"></i> <span class="d-none d-sm-none d-md-inline-block d-lg-inline-block">&nbsp;<?=$lang['save_changes']?></span></button>&nbsp;&nbsp;
                        <?=($permisos_tabla['puede_eliminar']) ? '<a class="button delete-button alt-button-1" href="#" data-id="'.$id.'" data-table="'.$info_tabla['ajustes_tabla']['id_tabla'].'"><svg aria-hidden="true" data-prefix="fal" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="icon-img-s vg-inline--fa fa-trash-alt fa-w-14 fa-2x"><path fill="currentcolor" d="M336 64l-33.6-44.8C293.3 7.1 279.1 0 264 0h-80c-15.1 0-29.3 7.1-38.4 19.2L112 64H24C10.7 64 0 74.7 0 88v2c0 3.3 2.7 6 6 6h26v368c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V96h26c3.3 0 6-2.7 6-6v-2c0-13.3-10.7-24-24-24h-88zM184 32h80c5 0 9.8 2.4 12.8 6.4L296 64H152l19.2-25.6c3-4 7.8-6.4 12.8-6.4zm200 432c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V96h320v368zm-176-44V156c0-6.6 5.4-12 12-12h8c6.6 0 12 5.4 12 12v264c0 6.6-5.4 12-12 12h-8c-6.6 0-12-5.4-12-12zm-80 0V156c0-6.6 5.4-12 12-12h8c6.6 0 12 5.4 12 12v264c0 6.6-5.4 12-12 12h-8c-6.6 0-12-5.4-12-12zm160 0V156c0-6.6 5.4-12 12-12h8c6.6 0 12 5.4 12 12v264c0 6.6-5.4 12-12 12h-8c-6.6 0-12-5.4-12-12z" class=""></path></svg><span class="d-none d-sm-none d-md-inline-block d-lg-inline-block">&nbsp;&nbsp;'.$lang['delete_button'].'</span></a>' : null?>
                        <?=($permisos_tabla['puede_imprimir']) ? '&nbsp;&nbsp;<a class="button print-button alt-button-2" href="javascript:window.print();"><svg aria-hidden="true" data-prefix="fal" data-icon="print" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="icon-img-s vg-inline--fa fa-print fa-w-16 fa-2x"><path fill="currentcolor" d="M416 192V81.9c0-6.4-2.5-12.5-7-17L351 7c-4.5-4.5-10.6-7-17-7H120c-13.2 0-24 10.8-24 24v168c-53 0-96 43-96 96v136c0 13.2 10.8 24 24 24h72v40c0 13.2 10.8 24 24 24h272c13.2 0 24-10.8 24-24v-40h72c13.2 0 24-10.8 24-24V288c0-53-43-96-96-96zM128 32h202.8L384 85.2V256H128V32zm256 448H128v-96h256v96zm96-64h-64v-40c0-13.2-10.8-24-24-24H120c-13.2 0-24 10.8-24 24v40H32V288c0-35.3 28.7-64 64-64v40c0 13.2 10.8 24 24 24h272c13.2 0 24-10.8 24-24v-40c35.3 0 64 28.7 64 64v128zm-28-112c0 11-9 20-20 20s-20-9-20-20 9-20 20-20 20 9 20 20z" class=""></path></svg><span class="d-none d-sm-none d-md-inline-block d-lg-inline-block">&nbsp;&nbsp;'.$lang['print_button'].'</span></a>' : null?>
                      </div>
                    </div>
                  </div>
                </div>

                <?php
                  if($new === false && $id != null) {
                    $table_pk = $cms->getPk($info_tabla['ajustes_tabla']['nombre_tabla']);
                    $registro = $cms->db->runQuery('SELECT ? FROM ? WHERE ? = ?',[$table_pk,$info_tabla['ajustes_tabla']['nombre_tabla'],$table_pk,$id]);
                    if(!is_array($registro)) {
                      throw new \Exception($lang['item_not_found'], 1);
                    }
                    else {
                      ?>
                        <?php
                        $editor_tabs = new Plantilla($nivel_base.'system/templates/partials/editor_content.tpl');
                        $editor_tabs->set('id',$id);
                        $editor_tabs->set('table_pk',$table_pk);
                        $editor_tabs->set('cms',$cms);
                        $editor_tabs->set('lang',$lang);
                        $editor_tabs->set('info_tabla',$info_tabla);
                        echo $editor_tabs->render();
                        ?>
                      <?php
                    }
                  }
                ?>
                <form action="requests/modify_registry.php" method="post">
                <?php
              }
              else {
                guardarLog('Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)',true);
                throw new \Exception($lang['error_table_settings'], 1);
              }


            }
            else {
              throw new \Exception($lang['no_permissions_to_edit'], 1);
            }
          }


        }

      }
      else {
        throw new \Exception($lang['no_table_selected'], 1);
      }
    } catch (\Exception $e) {
      ?>
      <div id="gradient"></div>
        <div class="container">
          <div id="generic-section" class="message radius">
          <div class="fs18 mbot10"><?=$e->getMessage()?></div>
          <a href="../admin" class="button"><i class="fas fa-reply"></i>&nbsp;&nbsp;<?=$lang['back_home_text']?></a>
          </div>
        </div>
      <?php
    }
    ?>
</div>
