<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=${'error_connection_title'}?></title>
    <style>
      html, body {
        font-family: 'Open Sans',sans-serif;
        margin:0;
        padding:0;
        background:#F1F3F4;
      }
    </style>
  </head>
  <body>
      <div style="padding:5px;background:#FFF;margin:10% auto;width:900px;max-width:calc(100% - 40px);">
          <div style="border:1px dashed #ebebeb;padding:30px;font-weight:600;">
            <?=${'error_connection_body'}?>
          </div>
      </div>
  </body>
</html>
