$(document).ready(function() {
  $.fn.isInViewportVer = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  $.fn.isInViewportHorStrict = function() {
    var elementLeft = $(this).offset().left;
    var elementRight = elementLeft + $(this).outerWidth();
    var parentLeft = $(this).parent().offset().left;
    var parentRight = parentLeft + $(this).parent().width();
    return elementRight  > parentLeft  && elementLeft + $(this).width() + 30 < parentRight;
  };

  $.fn.exists = function() {
    return ($(this).length > 0) ? true : false;
  };


  function fixDoc() {
      var h = $(window).height();
      $('body').css('min-height', h);
  }

  function detectTabs(){
    if($('.editor-tab').length) {
      var pCont = $('#pending-tabs');
      pCont.hide().html('');
      $('#editor-tabs > .editor-tab').each(function(){
        if(!$(this).isInViewportHorStrict()) {
          $(this).hide();
          pCont.append($(this).clone());
        }
        else {
          $(this).show();
        }
      });
      if(pCont.html() != '') {
        $('#show-tabs').show();
      }
      else {
        $('#show-tabs').hide();
      }
    }
  }

  function moveTo() {
    $('.moveto').each(function() {
      var target = $(this).attr('data-target');
      $(this).appendTo(target);
    });
  }




  function resizeLayer() {
    $('.res-layer').each(function() { // Square Rendering
      var ancho_ventana = $(window).width();
      var ancho = $(this).width();
      if ($(this).attr('data-rel') == null) {
        relacion = 1;
      } else {
        relacion = parseFloat($(this).attr('data-rel'));
        if (ancho_ventana > 1024) { // Si es mayor a 1024px...
          relacion = parseFloat($(this).attr('data-rel'));
        } else { // Si no... veo si estÃƒÂ¡ la relaciÃƒÂ³n en los tablets o mÃƒÂ³viles...
          relacion_tablet = parseFloat($(this).attr('data-rel-tablet'));
          if (isNaN(relacion_tablet) !== true) {
            relacion = relacion_tablet;
          }
          if (ancho_ventana <= 640) { // Si es mÃƒÂ³vil...
            relacion_movil = parseFloat($(this).attr('data-rel-mobile'));
            if (isNaN(relacion_movil) !== true) {
              relacion = relacion_movil;
            }
          }
        }
      }
      $(this).css('height', ancho * relacion);
    });
  }

  fixDoc();
  detectTabs();
  resizeLayer();
  moveTo();
  $(window).resize(function() {
    fixDoc();
    resizeLayer();
    detectTabs();
  });

  if ($('textarea.rich').exists()) {
    tinymce.init({
      selector: 'textarea.rich',
      language: system_lang,
      readonly: 0,
      branding: false,
      height:200,
      statusbar: false,
      autoresize_bottom_margin: 10,
      menubar: 'edit insert format table tools',
      plugins: 'autolink link lists textcolor table hr autoresize image',
      toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | hr | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | table | removeformat',
      toolbar1: 'undo redo | copy cut paste | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | removeformat',
      toolbar2: 'hr link | numlist bullist outdent indent  | table | formatselect | image',
      relative_urls : false,
      remove_script_host : false,
      convert_urls : true,
      images_upload_url: nivel_base+'requests/editor_upload_image.php',
      images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', nivel_base+'requests/editor_upload_image.php');
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
      },
      setup: function(ed) {
        if ($('#' + ed.id).prop('readonly')) {
          ed.settings.readonly = true;
        }
      }
    });
  }

  $(document).on('click','#show-tabs',function(e){
    e.preventDefault();
    $('#pending-tabs').toggle();
  });

  $(document).on('click',function(e){
    if(!$(e.target).is('#show-tabs')) {
      $('#pending-tabs').hide();
    }
  });


  $(document).on('click','.editor-tab',function(e){
    e.preventDefault();
    var target = $(this).attr('data-target');
    $('.editor-tab').removeClass('active');
    $(this).addClass('active');
    $('.tab-content').removeClass('active');
    $(target).addClass('active');
    tinyMCE.execCommand("mceRepaint");
    resizeLayer();
  });

  $('.toggle-password').click(function(e) {
    e.preventDefault();
    $('body').toggleClass('show-remember');
  });
  /*Header menu */
  $(document).on('click', '.toggle-header-menu', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('.user-menu').fadeToggle(0).toggleClass('active');
  });
  $(document).on('click', function(e) {
    $('.user-menu').fadeOut(0).removeClass('active');
  });
  $(document).on('click', '.user-menu', function(e) {
    e.stopPropagation();
  });


  $(document).on('click', '.toggle-notifications', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('#notifications').fadeToggle(0).toggleClass('active');
  });
  $(document).on('click', function(e) {
    $('#notifications').fadeOut(0).removeClass('active');
  });
  $(document).on('click', '#notifications', function(e) {
    e.stopPropagation();
  });
  /* End Header menu */
  Notification.requestPermission().then(function(result) {
    if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      // var notification = new Notification("Hi there!");
    }
  });
  $(".timeago").timeago();
  $('#sidebar-tables ul li').each(function() {
    if ($(this).find('.sidebar-link.active').length) {
      $(this).addClass('toggle-active');
    }
  });

  if($('.rut-check').length) {
    $.getScript( nivel_base+"vendors/jquery.rut-master/jquery.rut.js", function( data, textStatus, jqxhr ) {
      if(jqxhr.status == 200) {

        $('.rut-check').each(function(){
          var rut = $(this).val();
          el = $(this).attr('id');
          el = $("#"+el)[0];
          if(rut.length > 0) {
            if($.validateRut(rut)) {
	             el.setCustomValidity("");
             }
             else {
               el.setCustomValidity(lang.invalid_chilean_rut);
             }
          }
        });

        $(".rut-check").rut({
        	formatOn: 'keyup keydown change',
            minimumLength: 8, // validar largo mínimo; default: 2
        	   validateOn: 'change blur' // si no se quiere validar, pasar null
           }).on('rutInvalido', function(e) {
             el = e.currentTarget;
             el = $(el).attr('id');
             el = $("#"+el)[0];
             el.setCustomValidity(lang.invalid_chilean_rut);
            }).on('rutValido', function(e, rut, dv) {
              el = e.currentTarget;
              el = $(el).attr('id');
              el = $("#"+el)[0];
              el.setCustomValidity("");
            });
      }
    });
  }
  if($('.tag-input').length) {
    $.getScript( nivel_base+"vendors/tags-input/jquery.tagsinput.min.js", function( data, textStatus, jqxhr ) {
      if(jqxhr.status == 200) {
        $('.tag-input').tagsInput({
          'defaultText':lang.add_tags
        });
      }
    });
  }
  if($('.clock-picker').length) {
    $.getScript( nivel_base+"vendors/jquery.wickedpicker/wickedpicker.min.js", function( data, textStatus, jqxhr ) {
      var css_link = $("<link>", {
        rel: "stylesheet",
        type: "text/css",
        href: nivel_base+"vendors/jquery.wickedpicker/wickedpicker.min.css"
      });
      //css_link.appendTo('head');
      if(jqxhr.status == 200) {
        $('.clock-picker').wickedpicker({
          twentyFour: true,
          title: lang.select_hour
        });
      }
    });
  }
  $('.delete-button').click(function(e){
    e.preventDefault();
    var table_id = ($(this).attr('data-table') != null) ? $(this).attr('data-table') : null;
    var id = ($(this).attr('data-id') != null) ? $(this).attr('data-id') : null;
    swal({
    title: lang.remove_item_conf,
    text: lang.remove_item_desc,
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: lang.delete_button,
    cancelButtonText: lang.cancel_button
  }).then((result) => {
    if (result.value) {
      swal({
      title: lang.done_label,
      text: lang.registry_removed_succ,
      type: 'success',
      confirmButtonText: lang.accept_button
    })
    }
  })
  });

  function getInvalidInputs() { // Obtengo los inputs inválidos para cada pestaña
    var cg = 0;
    $('.tab-content').each(function(){
      var id = $(this).attr('id');
      var c = 0;
      var inp = $(this).find(':input');
      if(inp.length > 0) {
        inp.each(function(){
          if($(this).is(":invalid")) {
              c++;
              cg++;
          }
        });
      }
      tab = $('.editor-tab[data-target="#'+id+'"]');
      if(c> 0) {
        tab.find('.error').html(c).show().addClass('force-inline-block');
      }
      else {
        tab.find('.error').html(0).hide().removeClass('force-inline-block');
      }
    });
    return cg;
  }

  if($('.tab-content').length) {
    setTimeout(function(){ getInvalidInputs();}, 500);
  }


  $(document).on('change keydown keyup blur',"#editor-form :input",function(){
    getInvalidInputs();
  });

  $('#ed-save').click(function(e){
      count = getInvalidInputs();
      if(count != 0) {
        e.preventDefault();
        swal({
          title: lang.error_title,
          text: lang.fix_inputs_message,
          type: 'warning',
          confirmButtonText: lang.accept_button
        });
      }
  });


  if($('.date-picker').length) {
    $.getScript("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/i18n/jquery-ui-i18n.min.js", function( data, textStatus, jqxhr ) {
      if(jqxhr.status == 200) {
        $.datepicker.setDefaults($.datepicker.regional[lang.lang_prefix_editor]);
        $( ".date-picker" ).datepicker({
          dateFormat: 'yy-mm-dd'
        });
      }
    });
  }
});
