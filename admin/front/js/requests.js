$(document).ready(function(){

  $(document).on('click','.single-notification',function(){
    alert('asda');
  });

  $(document).on('click','.acc-sett-go',function(e){
    e.preventDefault();
    $('#editorform').remove();
      $('body').append('<div id="editorform" class="small-modal hidden-in-body">\
      <div class="editor-head text-center relative">\
      <img id="prof_editor_img" class="absolute circle" src="'+user_data.avatar+'">\
      </div>\
      <form id="editor-form" action="'+nivel_base+'requests/edit_account.php">\
      <label class="block nomargin fssmall">'+lang['acc_name_pl']+'</label>\
      <input name="name" value="'+user_data.nombre+'" type="text" class="fwidth mbot10" placeholder="'+lang['acc_name_pl']+'">\
      <label class="block nomargin fssmall">'+lang['acc_lname_pl']+'</label>\
      <input name="lname" value="'+user_data.apellido+'" type="text" class="fwidth mbot10" placeholder="'+lang['acc_lname_pl']+'">\
      <label class="block nomargin fssmall">'+lang['acc_phone_pl']+'</label>\
      <input name="email" value="'+user_data.telefono+'" type="text" class="fwidth mbot10" placeholder="'+lang['acc_phone_pl']+'">\
      <label class="block nomargin fssmall">'+lang['acc_email_pl']+'</label>\
      <input name="email" value="'+user_data.email+'" type="text" class="fwidth mbot10" placeholder="'+lang['acc_email_pl']+'">\
      <input class="button mbot20" type="submit" value="'+lang['edit_acc_btn']+'">\
      </form>\
      </div>');
    $.featherlight($('#editorform'), null);
  })

  // $('.acc-sett-go').click();


  function requestNotifications() {
    var count_holder = $('.notif-balloon');
    var notif_holder = $('#notifications');
    if(!count_holder.length || user_signed_in == false) {
      return false;
    }
    $.ajax({
      type: 'GET',
      url: nivel_base+'requests/notifications.php',
      contentType: "application/json",
      dataType: 'json',
      async: true,
      success: function(data) {
        switch (data['success']) {
          case true:
            if(data['data']['total'] == 0) { // No se encontraron notificaciones...
              count_holder.hide();
              notif_holder.html('<div class="loading-notifications text-center">'+lang['no_notifications']+'</div>');
            }
            else {
              count_holder.show().html(data['data']['pending']);
              var html = '<div id="notif-title" class="text-left">'+lang['notifications_label']+' ('+data['data']['pending']+')</div>';
              $.each(data['data']['notifications'], function(index, notif) {
                if(index+1 > 4) { // Para 4...
                  return false;
                }
                html+= '<div class="single-notification text-left nf '+(notif['leido'] == true ? null : 'not-readed')+'">\
                <div '+(notif['url_thumbnail'] != null ? 'class="notif-with-thumb"' : '')+'>\
                <div class="notification-title">'+notif['nombre']+' &bull; <time datetime="'+notif['fecha_creacion'].date+'" class="timeago"></time></div>\
                <div class="notification-text fs12">'+notif['texto'].substring(0, 60)+'...</div>\
                </div>\
                '+(notif['url_thumbnail'] != null ? '<div class="notif-thumbnail"><img src="'+nivel_base+'../uploads/notifications_thumbs/'+notif['url_thumbnail']+'" class="responsive valign"></div>' : '')+'\
                <div class="clearfix"></div></div>';
              });
              notif_holder.html(html);
              $("time.timeago").timeago();
            }
          break;
          case false: // Error
              alert(data['message']);
          break;
          default:

        }
      },
      error: function(response) {
        // alert(lang.cant_reach_request);
      }
    });

  }

  requestNotifications();
  setInterval(function(){
    requestNotifications();
  }, 3000); // Default 3s




});
