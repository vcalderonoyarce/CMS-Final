-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 25-06-2018 a las 23:23:58
-- Versión del servidor: 5.7.21
-- Versión de PHP: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cms_proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_ajustes_globales`
--

DROP TABLE IF EXISTS `cms_ajustes_globales`;
CREATE TABLE IF NOT EXISTS `cms_ajustes_globales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_por_defecto` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `nombre_sitio` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `campos_checkbox` text COLLATE utf8mb4_spanish2_ci,
  `campos_enriquecidos` text COLLATE utf8mb4_spanish2_ci,
  `campos_rut` text COLLATE utf8mb4_spanish2_ci,
  `campos_ubicacion` text COLLATE utf8mb4_spanish2_ci,
  `campos_imagen` text COLLATE utf8mb4_spanish2_ci,
  `campos_archivo` text COLLATE utf8mb4_spanish2_ci,
  `campos_password` text COLLATE utf8mb4_spanish2_ci,
  `campos_tag` text COLLATE utf8mb4_spanish2_ci,
  `campos_hora` text COLLATE utf8mb4_spanish2_ci,
  `campos_fecha` text COLLATE utf8mb4_spanish2_ci,
  `campos_fecha_hora` text COLLATE utf8mb4_spanish2_ci,
  `campos_video_incrustado` text COLLATE utf8mb4_spanish2_ci,
  `campos_video` text COLLATE utf8mb4_spanish2_ci,
  `campos_audio` text COLLATE utf8mb4_spanish2_ci,
  `campos_tarjeta_credito` text COLLATE utf8mb4_spanish2_ci,
  `modo_sandbox` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_ajustes_globales`
--

INSERT INTO `cms_ajustes_globales` (`id`, `idioma_por_defecto`, `nombre_sitio`, `campos_checkbox`, `campos_enriquecidos`, `campos_rut`, `campos_ubicacion`, `campos_imagen`, `campos_archivo`, `campos_password`, `campos_tag`, `campos_hora`, `campos_fecha`, `campos_fecha_hora`, `campos_video_incrustado`, `campos_video`, `campos_audio`, `campos_tarjeta_credito`, `modo_sandbox`) VALUES
(1, 'es', 'Nombre sitio', 'activo,publicado,requerido,activado,inactivo,es_requerido,excluyente,pagado,completo,visible', 'descripcion,detalles,redaccion,instrucciones', 'rut', 'ubicacion_geografica,geolocalizacion,mapa_coordenadas', 'imagen,thumbnail,miniatura,avatar', 'archivo', 'contrasena', 'etiquetas,tags', 'hora', 'fecha', 'fecha_creacion,fecha_modificacion', NULL, 'video', NULL, 'credit_card,tarjeta_de_credito,tarjeta_credito', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_estados_notificaciones`
--

DROP TABLE IF EXISTS `cms_estados_notificaciones`;
CREATE TABLE IF NOT EXISTS `cms_estados_notificaciones` (
  `cms_notificaciones_id` int(11) DEFAULT NULL,
  `cms_usuarios_id` int(11) DEFAULT NULL,
  `leido` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_estados_notificaciones`
--

INSERT INTO `cms_estados_notificaciones` (`cms_notificaciones_id`, `cms_usuarios_id`, `leido`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

DROP TABLE IF EXISTS `cms_logs`;
CREATE TABLE IF NOT EXISTS `cms_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8mb4_spanish2_ci,
  `error` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=423 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `texto`, `error`, `fecha_creacion`) VALUES
(14, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-25 15:05:15'),
(13, 'El usuario con el id 1 inició sesión desde IP : ::1', 0, '2018-02-25 14:57:53'),
(12, 'El usuario con el id 1 inició sesión desde IP : ::1', 0, '2018-02-25 13:37:49'),
(11, 'El usuario con el id 1 cerró sesión desde IP : ::1', 0, '2018-02-25 13:37:47'),
(10, 'El usuario con el id 1 inició sesión desde IP : ::1', 0, '2018-02-25 13:24:27'),
(9, 'El usuario con el id 1 cerró sesión desde IP : ::1', 0, '2018-02-25 13:24:10'),
(8, 'El usuario con el id 1 inició sesión desde IP : ::1', 0, '2018-02-25 13:22:21'),
(15, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-25 15:25:41'),
(16, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-25 20:13:28'),
(17, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-25 21:10:32'),
(18, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-25 23:28:27'),
(19, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.44', 0, '2018-02-26 02:00:40'),
(20, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-26 02:01:08'),
(21, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-26 07:27:19'),
(22, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-26 12:16:57'),
(23, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-26 14:35:38'),
(24, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-26 16:29:17'),
(25, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.42', 0, '2018-02-26 16:38:56'),
(26, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-26 16:39:03'),
(27, 'Se intentó ingresar a una tabla que no ha sido inicializada (353 no existe en el sistema)', 1, '2018-02-26 16:52:30'),
(28, 'Se intentó ingresar a una tabla que no ha sido inicializada (353 no existe en el sistema), por el usuario 1', 1, '2018-02-26 16:53:09'),
(29, 'Se intentó ingresar a una tabla que no ha sido inicializada (353 no existe en el sistema), por el usuario ID: 1', 1, '2018-02-26 16:53:25'),
(30, 'Se intentó ingresar a una tabla que no ha sido inicializada (353 no existe en el sistema), por el usuario ID: 1', 1, '2018-02-26 16:53:30'),
(31, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 17:02:37'),
(32, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 17:07:02'),
(33, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 17:35:11'),
(34, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 22:20:23'),
(35, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 22:20:24'),
(36, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-26 22:45:55'),
(37, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:09:02'),
(38, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:09:04'),
(39, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:09:06'),
(40, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:14:49'),
(41, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:15:18'),
(42, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:15:51'),
(43, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:16:27'),
(44, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:16:51'),
(45, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:17:15'),
(46, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-26 23:17:18'),
(47, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-27 00:00:07'),
(48, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.42', 0, '2018-02-27 00:03:01'),
(49, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 00:08:10'),
(50, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.42', 0, '2018-02-27 00:08:14'),
(51, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-27 00:43:07'),
(52, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 01:08:24'),
(53, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 01:12:45'),
(54, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-27 01:54:53'),
(55, 'La tabla \"entradas\" no existe en la base de datos', 1, '2018-02-27 01:54:54'),
(56, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 10:19:56'),
(57, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-27 12:06:20'),
(58, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:19:46'),
(59, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:20:19'),
(60, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:46:05'),
(61, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:46:13'),
(62, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:58:32'),
(63, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 12:59:42'),
(64, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:02:39'),
(65, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:03:30'),
(66, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:04:31'),
(67, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:04:38'),
(68, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:04:48'),
(69, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:06:34'),
(70, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:06:54'),
(71, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:09:48'),
(72, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:11:10'),
(73, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:11:46'),
(74, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:11:55'),
(75, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:14:20'),
(76, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-02-27 13:15:58'),
(77, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:15:58'),
(78, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:16:07'),
(79, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:17:17'),
(80, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:18:51'),
(81, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:19:32'),
(82, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:19:44'),
(83, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:21:10'),
(84, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:21:19'),
(85, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:22:32'),
(86, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:23:04'),
(87, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:23:07'),
(88, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 13:24:58'),
(89, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-27 16:03:14'),
(90, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 16:03:15'),
(91, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 17:26:29'),
(92, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 17:26:33'),
(93, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-27 23:24:42'),
(94, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 23:26:05'),
(95, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 23:27:57'),
(96, 'Se produjo un error al intentar obtener los ajustes de la tabla (CMS->ajustesTabla)', 1, '2018-02-27 23:32:21'),
(97, 'El usuario con el id 1 inició sesión desde IP : 192.168.43.86', 0, '2018-02-28 00:31:14'),
(98, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-02-28 10:01:40'),
(99, 'El usuario con el id 1 inició sesión desde IP : 192.168.43.86', 0, '2018-02-28 14:50:09'),
(100, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-28 18:21:31'),
(101, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-28 21:19:52'),
(102, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-28 22:32:32'),
(103, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-02-28 23:54:35'),
(104, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.44', 0, '2018-03-01 10:10:32'),
(105, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-03-01 14:28:55'),
(106, 'El usuario con el id 2 inició sesión desde IP : 192.168.1.42', 0, '2018-03-01 23:44:53'),
(107, 'El usuario con el id 2 inició sesión desde IP : 192.168.1.42', 0, '2018-03-01 23:45:13'),
(108, 'El usuario con el id 2 cerró sesión desde IP : 192.168.1.42', 0, '2018-03-01 23:51:51'),
(109, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-03-01 23:51:56'),
(110, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-03-06 15:23:15'),
(111, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(112, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(113, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(114, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(115, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(116, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(117, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(118, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(119, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(120, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(121, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(122, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(123, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(124, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(125, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(126, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(127, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(128, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(129, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(130, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(131, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(132, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(133, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(134, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(135, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(136, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(137, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(138, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(139, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(140, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(141, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:35:52'),
(142, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(143, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(144, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(145, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(146, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(147, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(148, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(149, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(150, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(151, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(152, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(153, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(154, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(155, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(156, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(157, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(158, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(159, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(160, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(161, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(162, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(163, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(164, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(165, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(166, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(167, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(168, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(169, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(170, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(171, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(172, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:02'),
(173, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(174, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(175, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(176, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(177, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(178, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(179, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(180, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(181, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(182, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(183, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(184, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(185, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(186, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(187, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(188, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(189, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(190, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(191, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(192, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(193, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(194, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(195, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(196, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(197, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(198, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(199, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(200, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(201, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(202, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(203, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:36:43'),
(204, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(205, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(206, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(207, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(208, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(209, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(210, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(211, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(212, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(213, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(214, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(215, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(216, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(217, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(218, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(219, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(220, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(221, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(222, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(223, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(224, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(225, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(226, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(227, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(228, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(229, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(230, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(231, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(232, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(233, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(234, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:16'),
(235, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(236, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(237, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(238, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(239, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(240, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(241, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(242, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(243, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(244, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(245, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(246, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(247, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(248, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(249, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(250, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(251, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(252, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(253, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(254, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(255, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(256, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(257, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(258, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(259, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(260, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(261, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(262, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(263, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(264, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(265, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:38:31'),
(266, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(267, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(268, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(269, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(270, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(271, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(272, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(273, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(274, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(275, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(276, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(277, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(278, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(279, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(280, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(281, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(282, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(283, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(284, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(285, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(286, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(287, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(288, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(289, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(290, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(291, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(292, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(293, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(294, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(295, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(296, 'Se está intentando llamar a una llave primaria, cuya tabla, o campo nombre no existe', 1, '2018-03-06 19:39:45'),
(297, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.42', 0, '2018-03-08 22:34:09'),
(298, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.43', 0, '2018-03-13 13:25:29'),
(299, 'Se intentó ingresar a una tabla que no ha sido inicializada (5353 no existe en el sistema), por el usuario ID: 1', 1, '2018-03-13 13:25:45'),
(300, 'Se intentó ingresar a una tabla que no ha sido inicializada (5353 no existe en el sistema), por el usuario ID: 1', 1, '2018-03-13 13:25:52'),
(301, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.43', 0, '2018-03-13 13:26:04'),
(302, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.43', 0, '2018-03-13 17:39:18'),
(303, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.43', 0, '2018-03-13 23:15:53'),
(304, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.43', 0, '2018-03-14 00:42:31'),
(305, 'El usuario con el id 1 cerró sesión desde IP : 192.168.1.43', 0, '2018-03-14 01:34:17'),
(306, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.43', 0, '2018-03-14 13:53:53'),
(307, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.39', 0, '2018-03-18 23:50:49'),
(308, 'El usuario con el id 1 inició sesión desde IP : 192.168.1.35', 0, '2018-03-19 18:37:08'),
(309, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-04-29 11:03:21'),
(310, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-04-29 11:04:00'),
(311, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-04-30 09:39:44'),
(312, 'El usuario con el id 1 cerró sesión desde IP : 192.168.137.1', 0, '2018-04-30 11:10:59'),
(313, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-04-30 11:11:07'),
(314, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-04-30 18:23:39'),
(315, 'Se intentó ingresar a una tabla que no ha sido inicializada (6 no existe en el sistema), por el usuario ID: 1', 1, '2018-04-30 23:10:13'),
(316, 'El usuario con el id 1 cerró sesión desde IP : 192.168.137.1', 0, '2018-05-01 10:49:09'),
(317, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-01 10:49:38'),
(318, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-01 13:39:51'),
(319, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-01 14:31:37'),
(320, 'Se intentó ingresar a una tabla que no ha sido inicializada (99 no existe en el sistema), por el usuario ID: 1', 1, '2018-05-01 14:36:42'),
(321, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-01 19:35:40'),
(322, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-02 01:26:22'),
(323, 'El usuario con el id 1 inició sesión desde IP : 192.168.137.1', 0, '2018-05-02 19:48:39'),
(324, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-03 12:06:21'),
(325, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-03 19:17:13'),
(326, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-04 22:55:37'),
(327, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-05 17:36:36'),
(328, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:13'),
(329, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:14'),
(330, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:14'),
(331, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:14'),
(332, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:15'),
(333, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:15'),
(334, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:15'),
(335, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:15'),
(336, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:16'),
(337, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:16'),
(338, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:16'),
(339, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:16'),
(340, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:18'),
(341, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:19'),
(342, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:19'),
(343, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:19'),
(344, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:20'),
(345, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:20'),
(346, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:20'),
(347, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:20'),
(348, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:20'),
(349, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:21'),
(350, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:21'),
(351, 'Se produjo un error crítico al intentar obtener los ajustes del sistema', 1, '2018-05-05 18:34:21'),
(352, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-05 18:36:58'),
(353, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-06 11:38:47'),
(354, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-06 11:41:28'),
(355, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-06 11:42:31'),
(356, 'El usuario con el id 1 inició sesión desde IP : 192.168.0.16', 0, '2018-05-06 11:43:17'),
(357, NULL, 0, '2018-05-06 11:45:25'),
(358, NULL, 0, '2018-05-06 11:46:04'),
(359, NULL, 0, '2018-05-06 11:46:18'),
(360, NULL, 0, '2018-05-06 18:13:43'),
(361, NULL, 0, '2018-05-06 18:20:55'),
(362, NULL, 0, '2018-05-06 18:45:50'),
(363, NULL, 0, '2018-05-06 18:45:51'),
(364, NULL, 0, '2018-05-07 17:00:05'),
(365, NULL, 0, '2018-05-08 11:10:23'),
(366, NULL, 1, '2018-05-08 15:51:58'),
(367, NULL, 0, '2018-05-08 16:30:13'),
(368, NULL, 1, '2018-05-08 22:55:30'),
(369, NULL, 1, '2018-05-08 22:56:42'),
(370, NULL, 1, '2018-05-08 22:58:40'),
(371, NULL, 1, '2018-05-08 23:03:41'),
(372, NULL, 1, '2018-05-08 23:12:34'),
(373, NULL, 1, '2018-05-08 23:15:11'),
(374, NULL, 1, '2018-05-08 23:15:19'),
(375, NULL, 1, '2018-05-08 23:15:34'),
(376, NULL, 0, '2018-05-09 16:32:38'),
(377, NULL, 0, '2018-05-09 16:32:49'),
(378, NULL, 0, '2018-05-09 16:52:45'),
(379, NULL, 1, '2018-05-09 17:33:04'),
(380, NULL, 0, '2018-05-10 11:20:37'),
(381, NULL, 0, '2018-05-10 14:54:20'),
(382, NULL, 0, '2018-05-10 14:56:44'),
(383, NULL, 0, '2018-05-10 16:05:23'),
(384, NULL, 0, '2018-05-10 16:15:19'),
(385, NULL, 0, '2018-05-10 17:17:54'),
(386, NULL, 0, '2018-05-11 01:22:13'),
(387, NULL, 0, '2018-05-11 21:16:27'),
(388, NULL, 0, '2018-05-11 21:54:59'),
(389, NULL, 0, '2018-05-12 16:41:12'),
(390, NULL, 0, '2018-05-12 16:41:24'),
(391, NULL, 0, '2018-05-12 21:03:14'),
(392, NULL, 0, '2018-05-13 01:47:26'),
(393, NULL, 0, '2018-05-13 11:22:27'),
(394, NULL, 1, '2018-05-13 23:35:47'),
(395, NULL, 1, '2018-05-13 23:37:39'),
(396, NULL, 1, '2018-05-13 23:38:14'),
(397, NULL, 1, '2018-05-13 23:39:01'),
(398, NULL, 1, '2018-05-13 23:39:12'),
(399, 'asdasd', 1, '2018-05-13 23:39:38'),
(400, NULL, 1, '2018-05-13 23:40:09'),
(401, NULL, 1, '2018-05-13 23:44:10'),
(402, NULL, 0, '2018-05-14 00:02:02'),
(403, NULL, 0, '2018-05-14 11:32:58'),
(404, NULL, 0, '2018-05-14 20:06:33'),
(405, NULL, 0, '2018-05-15 00:06:18'),
(406, NULL, 0, '2018-05-16 11:48:59'),
(407, NULL, 0, '2018-05-16 11:49:03'),
(408, NULL, 0, '2018-05-16 11:49:14'),
(409, NULL, 0, '2018-05-17 20:56:37'),
(410, NULL, 0, '2018-05-18 18:26:37'),
(411, NULL, 0, '2018-05-21 14:48:41'),
(412, NULL, 0, '2018-05-22 11:14:35'),
(413, NULL, 0, '2018-05-22 14:06:57'),
(414, NULL, 0, '2018-05-22 14:19:58'),
(415, NULL, 0, '2018-05-22 14:26:23'),
(416, NULL, 0, '2018-05-22 14:36:28'),
(417, NULL, 0, '2018-05-22 14:36:45'),
(418, NULL, 0, '2018-05-22 14:36:55'),
(419, NULL, 0, '2018-05-28 19:28:01'),
(420, NULL, 0, '2018-06-05 12:19:50'),
(421, NULL, 0, '2018-06-12 20:02:28'),
(422, NULL, 0, '2018-06-25 14:55:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notificaciones`
--

DROP TABLE IF EXISTS `cms_notificaciones`;
CREATE TABLE IF NOT EXISTS `cms_notificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `texto` text COLLATE utf8mb4_spanish2_ci,
  `icono` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `color_etiqueta` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fecha_programacion` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_notificaciones`
--

INSERT INTO `cms_notificaciones` (`id`, `nombre`, `texto`, `icono`, `color_etiqueta`, `fecha_programacion`, `fecha_creacion`, `fecha_modificacion`) VALUES
(1, 'Notificación de prueba', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'fab fa-twitter', '#ddd', '2018-02-05 00:00:00', '2018-02-24 14:57:05', NULL),
(2, 'Notificación de prueba', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, '#92d1c8', '2018-02-05 00:00:00', '2018-02-24 14:57:05', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notificaciones_perfiles`
--

DROP TABLE IF EXISTS `cms_notificaciones_perfiles`;
CREATE TABLE IF NOT EXISTS `cms_notificaciones_perfiles` (
  `cms_notificaciones_id` int(11) DEFAULT NULL,
  `cms_perfiles_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_notificaciones_perfiles`
--

INSERT INTO `cms_notificaciones_perfiles` (`cms_notificaciones_id`, `cms_perfiles_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_opciones_tablas`
--

DROP TABLE IF EXISTS `cms_opciones_tablas`;
CREATE TABLE IF NOT EXISTS `cms_opciones_tablas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_tablas_id` int(11) DEFAULT NULL,
  `ajuste` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `valor_ajuste` text COLLATE utf8mb4_spanish2_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_opciones_tablas`
--

INSERT INTO `cms_opciones_tablas` (`id`, `cms_tablas_id`, `ajuste`, `valor_ajuste`) VALUES
(1, 3, 'campos_requeridos', 'nombre,rut,edad'),
(4, 2, 'campos_ocultos_lista', 'descripcion,tags,archivo. fecha_creacion, fecha_modificacion'),
(3, 2, 'campos_ocultos_editor', 'nombre,publicado,descripcion,fecha_creacion'),
(5, 3, 'campos_ocultos_lista', 'fecha_modificacion,cms_usuarios_id,fecha_creacion,descripcion'),
(6, 3, 'tabs', '{\r\n\"Campos multimedia\": [\"imagen\",\"video\"],\r\n\"Campos numéricos\": [\"numero_normal\",\"edad\"], \r\n\"Campos de texto\": [\"nombre\",\"tags\",\"descripcion\"],\r\n\"Campos de fecha\": [\"fecha\",\"hora\",\"fecha_completa\"], \r\n\"Campos seguridad\" : [\"tarjeta_credito\"],\r\n\"Campos extras\": [\"rut\"]}'),
(8, 3, 'formato_campos', '{\r\n\"edad\":{\"formato\":\"select\",\"minimo\":\"0\",\"maximo\":\"99\",\"salto\":\"1\"}\r\n,\r\n\"numero_normal\":{\"formato\":null,\"minimo\":\"0\",\"maximo\":\"999\"}\r\n}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_ortografia`
--

DROP TABLE IF EXISTS `cms_ortografia`;
CREATE TABLE IF NOT EXISTS `cms_ortografia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_idioma` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `buscar` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `reemplazar` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_ortografia`
--

INSERT INTO `cms_ortografia` (`id`, `codigo_idioma`, `buscar`, `reemplazar`) VALUES
(1, 'es', 'creacion', 'creación'),
(2, 'es', 'modificacion', 'modificación'),
(3, 'es', 'descripcion', 'descripción'),
(4, 'es', 'pais', 'país'),
(5, 'es', 'contrasena', 'contraseña'),
(6, 'es', 'credito', 'crédito'),
(7, 'es', 'video', 'vídeo'),
(10, 'es', 'numero', 'número'),
(9, 'es', 'modificacion', 'modificación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_perfiles`
--

DROP TABLE IF EXISTS `cms_perfiles`;
CREATE TABLE IF NOT EXISTS `cms_perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_perfiles`
--

INSERT INTO `cms_perfiles` (`id`, `nombre`) VALUES
(1, 'Usuario maestro'),
(2, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_tablas`
--

DROP TABLE IF EXISTS `cms_tablas`;
CREATE TABLE IF NOT EXISTS `cms_tablas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_real` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `nombre_plural` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `nombre_singular` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `mostrar_en_sidebar` int(11) DEFAULT '1',
  `registro_unico` int(11) DEFAULT '0',
  `por_usuario` int(11) DEFAULT '0',
  `filtro_query` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `icono_sidebar` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `padre_sidebar_id` int(11) DEFAULT NULL,
  `inhabilitar_click_sidebar` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_tablas`
--

INSERT INTO `cms_tablas` (`id`, `nombre_real`, `nombre_plural`, `nombre_singular`, `mostrar_en_sidebar`, `registro_unico`, `por_usuario`, `filtro_query`, `icono_sidebar`, `padre_sidebar_id`, `inhabilitar_click_sidebar`) VALUES
(1, 'entradas', 'entradas', 'entrada', 1, 0, 1, NULL, 'fas fa-shopping-bag', NULL, 1),
(2, 'entradas', 'entradas', 'entrada', 1, 0, 1, NULL, 'fas fa-file-alt', 1, 0),
(3, 'prueba', 'pruebas', 'prueba', 1, 0, 0, NULL, 'fas fa-cog', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_tablas_perfiles`
--

DROP TABLE IF EXISTS `cms_tablas_perfiles`;
CREATE TABLE IF NOT EXISTS `cms_tablas_perfiles` (
  `cms_perfiles_id` int(11) DEFAULT NULL,
  `cms_tablas_id` int(11) DEFAULT NULL,
  `puede_visualizar` int(11) DEFAULT NULL,
  `puede_agregar` int(11) DEFAULT NULL,
  `puede_modificar` int(11) DEFAULT NULL,
  `puede_eliminar` int(11) DEFAULT NULL,
  `puede_imprimir` int(11) DEFAULT NULL,
  `puede_descargar` int(11) DEFAULT NULL,
  `puede_ver_informe` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_tablas_perfiles`
--

INSERT INTO `cms_tablas_perfiles` (`cms_perfiles_id`, `cms_tablas_id`, `puede_visualizar`, `puede_agregar`, `puede_modificar`, `puede_eliminar`, `puede_imprimir`, `puede_descargar`, `puede_ver_informe`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1),
(1, 2, 1, 1, 1, 1, 1, 1, 1),
(1, 3, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_usuarios`
--

DROP TABLE IF EXISTS `cms_usuarios`;
CREATE TABLE IF NOT EXISTS `cms_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_perfiles_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `activo` int(1) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `recuperar_token` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `cms_idiomas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cms_usuarios`
--

INSERT INTO `cms_usuarios` (`id`, `cms_perfiles_id`, `nombre`, `apellido`, `avatar`, `activo`, `email`, `password`, `recuperar_token`, `fecha_creacion`, `fecha_modificacion`, `creado_por`, `modificado_por`, `cms_idiomas_id`) VALUES
(1, 1, 'Víctor M.', 'Calderón Oyarce', 'sample.jpg', 1, 'vcalderonoyarce@gmail.com', '$2y$10$U2VV0dr0ZDa0.4KEhV68Tehs.5me4pR76wtezy7bRJDGQW.EKcbJy', NULL, '2018-02-12 00:00:00', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

DROP TABLE IF EXISTS `entradas`;
CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estados_id` int(11) DEFAULT NULL,
  `publicado` int(1) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_spanish2_ci,
  `tags` text COLLATE utf8mb4_spanish2_ci,
  `archivo` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `cms_usuarios_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `estados_id`, `publicado`, `nombre`, `thumbnail`, `video`, `descripcion`, `tags`, `archivo`, `fecha_creacion`, `fecha_modificacion`, `fecha`, `hora`, `cms_usuarios_id`) VALUES
(1, 1, 1, 'Prueba', NULL, 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(2, 1, 1, 'Prueba', 'prueba.png', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.pdf', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(3, 1, 1, 'Prueba', 'prueba2.png', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.ppt', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(4, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.xls', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(5, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(6, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(7, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(8, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(9, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(10, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(11, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(12, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(13, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(14, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(15, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(16, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(17, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(18, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(19, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(20, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(21, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(22, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(23, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(24, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(25, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.psd', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(26, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(27, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.doc', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(28, 1, 1, 'Prueba', 'prueba.gif', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.png', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(29, 1, 1, 'Prueba', 'UI-conditions.png', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.xml', '2018-03-01 08:00:00', '2018-03-01 14:00:00', NULL, '07:30:13', 1),
(30, 1, 1, 'Prueba', 'cart.png', 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'archivo_prueba.dll', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1),
(31, 1, 1, 'Prueba', NULL, 'small.mp4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'asdasd,aasdasodj,asojdksakod,eokjtoasd,asodjaosd,aosidjasofnasd,asodjasokd', 'prueba.txt', '2018-03-01 08:00:00', '2018-03-01 14:00:00', '2018-03-14', '07:30:13', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`) VALUES
(1, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `iso`, `nombre`) VALUES
(1, 'AF', 'Afganistán'),
(2, 'AX', 'Islas Gland'),
(3, 'AL', 'Albania'),
(4, 'DE', 'Alemania'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antártida'),
(9, 'AG', 'Antigua y Barbuda'),
(10, 'AN', 'Antillas Holandesas'),
(11, 'SA', 'Arabia Saudí'),
(12, 'DZ', 'Argelia'),
(13, 'AR', 'Argentina'),
(14, 'AM', 'Armenia'),
(15, 'AW', 'Aruba'),
(16, 'AU', 'Australia'),
(17, 'AT', 'Austria'),
(18, 'AZ', 'Azerbaiyán'),
(19, 'BS', 'Bahamas'),
(20, 'BH', 'Bahréin'),
(21, 'BD', 'Bangladesh'),
(22, 'BB', 'Barbados'),
(23, 'BY', 'Bielorrusia'),
(24, 'BE', 'Bélgica'),
(25, 'BZ', 'Belice'),
(26, 'BJ', 'Benin'),
(27, 'BM', 'Bermudas'),
(28, 'BT', 'Bhután'),
(29, 'BO', 'Bolivia'),
(30, 'BA', 'Bosnia y Herzegovina'),
(31, 'BW', 'Botsuana'),
(32, 'BV', 'Isla Bouvet'),
(33, 'BR', 'Brasil'),
(34, 'BN', 'Brunéi'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'CV', 'Cabo Verde'),
(39, 'KY', 'Islas Caimán'),
(40, 'KH', 'Camboya'),
(41, 'CM', 'Camerún'),
(42, 'CA', 'Canadá'),
(43, 'CF', 'República Centroafricana'),
(44, 'TD', 'Chad'),
(45, 'CZ', 'República Checa'),
(46, 'CL', 'Chile'),
(47, 'CN', 'China'),
(48, 'CY', 'Chipre'),
(49, 'CX', 'Isla de Navidad'),
(50, 'VA', 'Ciudad del Vaticano'),
(51, 'CC', 'Islas Cocos'),
(52, 'CO', 'Colombia'),
(53, 'KM', 'Comoras'),
(54, 'CD', 'República Democrática del Congo'),
(55, 'CG', 'Congo'),
(56, 'CK', 'Islas Cook'),
(57, 'KP', 'Corea del Norte'),
(58, 'KR', 'Corea del Sur'),
(59, 'CI', 'Costa de Marfil'),
(60, 'CR', 'Costa Rica'),
(61, 'HR', 'Croacia'),
(62, 'CU', 'Cuba'),
(63, 'DK', 'Dinamarca'),
(64, 'DM', 'Dominica'),
(65, 'DO', 'República Dominicana'),
(66, 'EC', 'Ecuador'),
(67, 'EG', 'Egipto'),
(68, 'SV', 'El Salvador'),
(69, 'AE', 'Emiratos Árabes Unidos'),
(70, 'ER', 'Eritrea'),
(71, 'SK', 'Eslovaquia'),
(72, 'SI', 'Eslovenia'),
(73, 'ES', 'España'),
(74, 'UM', 'Islas ultramarinas de Estados Unidos'),
(75, 'US', 'Estados Unidos'),
(76, 'EE', 'Estonia'),
(77, 'ET', 'Etiopía'),
(78, 'FO', 'Islas Feroe'),
(79, 'PH', 'Filipinas'),
(80, 'FI', 'Finlandia'),
(81, 'FJ', 'Fiyi'),
(82, 'FR', 'Francia'),
(83, 'GA', 'Gabón'),
(84, 'GM', 'Gambia'),
(85, 'GE', 'Georgia'),
(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur'),
(87, 'GH', 'Ghana'),
(88, 'GI', 'Gibraltar'),
(89, 'GD', 'Granada'),
(90, 'GR', 'Grecia'),
(91, 'GL', 'Groenlandia'),
(92, 'GP', 'Guadalupe'),
(93, 'GU', 'Guam'),
(94, 'GT', 'Guatemala'),
(95, 'GF', 'Guayana Francesa'),
(96, 'GN', 'Guinea'),
(97, 'GQ', 'Guinea Ecuatorial'),
(98, 'GW', 'Guinea-Bissau'),
(99, 'GY', 'Guyana'),
(100, 'HT', 'Haití'),
(101, 'HM', 'Islas Heard y McDonald'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong'),
(104, 'HU', 'Hungría'),
(105, 'IN', 'India'),
(106, 'ID', 'Indonesia'),
(107, 'IR', 'Irán'),
(108, 'IQ', 'Iraq'),
(109, 'IE', 'Irlanda'),
(110, 'IS', 'Islandia'),
(111, 'IL', 'Israel'),
(112, 'IT', 'Italia'),
(113, 'JM', 'Jamaica'),
(114, 'JP', 'Japón'),
(115, 'JO', 'Jordania'),
(116, 'KZ', 'Kazajstán'),
(117, 'KE', 'Kenia'),
(118, 'KG', 'Kirguistán'),
(119, 'KI', 'Kiribati'),
(120, 'KW', 'Kuwait'),
(121, 'LA', 'Laos'),
(122, 'LS', 'Lesotho'),
(123, 'LV', 'Letonia'),
(124, 'LB', 'Líbano'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libia'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lituania'),
(129, 'LU', 'Luxemburgo'),
(130, 'MO', 'Macao'),
(131, 'MK', 'ARY Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MY', 'Malasia'),
(134, 'MW', 'Malawi'),
(135, 'MV', 'Maldivas'),
(136, 'ML', 'Malí'),
(137, 'MT', 'Malta'),
(138, 'FK', 'Islas Malvinas'),
(139, 'MP', 'Islas Marianas del Norte'),
(140, 'MA', 'Marruecos'),
(141, 'MH', 'Islas Marshall'),
(142, 'MQ', 'Martinica'),
(143, 'MU', 'Mauricio'),
(144, 'MR', 'Mauritania'),
(145, 'YT', 'Mayotte'),
(146, 'MX', 'México'),
(147, 'FM', 'Micronesia'),
(148, 'MD', 'Moldavia'),
(149, 'MC', 'Mónaco'),
(150, 'MN', 'Mongolia'),
(151, 'MS', 'Montserrat'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NI', 'Nicaragua'),
(158, 'NE', 'Níger'),
(159, 'NG', 'Nigeria'),
(160, 'NU', 'Niue'),
(161, 'NF', 'Isla Norfolk'),
(162, 'NO', 'Noruega'),
(163, 'NC', 'Nueva Caledonia'),
(164, 'NZ', 'Nueva Zelanda'),
(165, 'OM', 'Omán'),
(166, 'NL', 'Países Bajos'),
(167, 'PK', 'Pakistán'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestina'),
(170, 'PA', 'Panamá'),
(171, 'PG', 'Papúa Nueva Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Perú'),
(174, 'PN', 'Islas Pitcairn'),
(175, 'PF', 'Polinesia Francesa'),
(176, 'PL', 'Polonia'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'GB', 'Reino Unido'),
(181, 'RE', 'Reunión'),
(182, 'RW', 'Ruanda'),
(183, 'RO', 'Rumania'),
(184, 'RU', 'Rusia'),
(185, 'EH', 'Sahara Occidental'),
(186, 'SB', 'Islas Salomón'),
(187, 'WS', 'Samoa'),
(188, 'AS', 'Samoa Americana'),
(189, 'KN', 'San Cristóbal y Nevis'),
(190, 'SM', 'San Marino'),
(191, 'PM', 'San Pedro y Miquelón'),
(192, 'VC', 'San Vicente y las Granadinas'),
(193, 'SH', 'Santa Helena'),
(194, 'LC', 'Santa Lucía'),
(195, 'ST', 'Santo Tomé y Príncipe'),
(196, 'SN', 'Senegal'),
(197, 'CS', 'Serbia y Montenegro'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leona'),
(200, 'SG', 'Singapur'),
(201, 'SY', 'Siria'),
(202, 'SO', 'Somalia'),
(203, 'LK', 'Sri Lanka'),
(204, 'SZ', 'Suazilandia'),
(205, 'ZA', 'Sudáfrica'),
(206, 'SD', 'Sudán'),
(207, 'SE', 'Suecia'),
(208, 'CH', 'Suiza'),
(209, 'SR', 'Surinam'),
(210, 'SJ', 'Svalbard y Jan Mayen'),
(211, 'TH', 'Tailandia'),
(212, 'TW', 'Taiwán'),
(213, 'TZ', 'Tanzania'),
(214, 'TJ', 'Tayikistán'),
(215, 'IO', 'Territorio Británico del Océano Índico'),
(216, 'TF', 'Territorios Australes Franceses'),
(217, 'TL', 'Timor Oriental'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad y Tobago'),
(222, 'TN', 'Túnez'),
(223, 'TC', 'Islas Turcas y Caicos'),
(224, 'TM', 'Turkmenistán'),
(225, 'TR', 'Turquía'),
(226, 'TV', 'Tuvalu'),
(227, 'UA', 'Ucrania'),
(228, 'UG', 'Uganda'),
(229, 'UY', 'Uruguay'),
(230, 'UZ', 'Uzbekistán'),
(231, 'VU', 'Vanuatu'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Islas Vírgenes Británicas'),
(235, 'VI', 'Islas Vírgenes de los Estados Unidos'),
(236, 'WF', 'Wallis y Futuna'),
(237, 'YE', 'Yemen'),
(238, 'DJ', 'Yibuti'),
(239, 'ZM', 'Zambia'),
(240, 'ZW', 'Zimbabue');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

DROP TABLE IF EXISTS `prueba`;
CREATE TABLE IF NOT EXISTS `prueba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo` int(1) DEFAULT NULL,
  `numero_normal` int(11) DEFAULT NULL COMMENT 'Número con rango entre 0 a 999',
  `edad` int(11) DEFAULT NULL COMMENT 'Campo numérico formato select, con salto en 1, rango de 0 a 99',
  `imagen` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL COMMENT 'Imagen sin ajustes (Básica)',
  `video` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `rut` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL COMMENT 'Campo tipo RUT chileno',
  `nombre` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL COMMENT 'Campo de texto genérico',
  `tags` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL COMMENT 'Campo tipo tag',
  `descripcion` text COLLATE utf8mb4_spanish2_ci COMMENT 'Texto enriquecido',
  `tarjeta_credito` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `contrasena` varchar(255) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `paises_id` int(11) DEFAULT NULL COMMENT 'Selecciona un país de la lista',
  `fecha_modificacion` datetime DEFAULT NULL,
  `cms_usuarios_id` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL COMMENT 'Campo de fecha (Sin picker de hora)',
  `hora` time DEFAULT NULL COMMENT 'Campo de hora (Sin picker de fecha)',
  `fecha_completa` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `prueba`
--

INSERT INTO `prueba` (`id`, `activo`, `numero_normal`, `edad`, `imagen`, `video`, `rut`, `nombre`, `tags`, `descripcion`, `tarjeta_credito`, `contrasena`, `fecha_creacion`, `paises_id`, `fecha_modificacion`, `cms_usuarios_id`, `fecha`, `hora`, `fecha_completa`) VALUES
(1, 1, NULL, 26, NULL, NULL, '18.126.083-1', 'Víctor Calderón Oyarce', NULL, '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'NytZWEJzZ2QvSUU2WWNBMGVZb1d5WWZjbkd6dEZEYmFzL2hIaUxvNUFKcz0=', 'asdasdasdasda', '2018-04-20 18:25:00', 46, NULL, 1, '2018-12-22', '00:00:00', '2018-05-10 00:00:00'),
(2, 1, NULL, 30, NULL, NULL, '16.715.383-6', 'Vania Calderón Zúñiga', NULL, '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'K3ptVXhZaUs5dzlJVHBGbTM1akkyR1ZqM3RydUZwb0NOaVgvVXY3L204dz0=', 'asfasdasdasd', NULL, 46, NULL, 1, '2018-12-22', '15:59:37', NULL),
(3, 1, NULL, 10, 'test.png', NULL, '22.689.933-2', 'Maite Tapia Calderón', NULL, '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'TVRsQUZ4U3RqdHk0NDJwQU1rN0dnUT09', 'asfasdasdasd', NULL, 46, NULL, 1, '2018-12-22', '15:59:37', NULL),
(4, 1, NULL, 49, NULL, NULL, '11.258.033-6', 'Marcela Oyarce Arenas', NULL, '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'dVZNOU5LNjN0azNSZjZjOUM0TlpQTlN5NnFoQWZYSFJaZFhlLzJydzNPdz0=', 'fasdasdasd', NULL, 46, NULL, 1, '2018-12-22', '15:59:37', NULL),
(5, 1, NULL, 21, NULL, NULL, '18.865.985-3', 'Diego Calderón Oyarce', NULL, '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'OEtqOFNQdVdJU1FXTTRFemd3YytmTXR3eUUvSXozWno1S2lBdUFJUjRHVT0=', 'asfasdasda', NULL, 46, NULL, 1, '2018-12-22', '15:59:37', NULL),
(6, 1, NULL, 24, 'test.png', 'small.mp4', '12.125.123-5', 'Prueba rut inválido', 'lorem,ipsum,dolor, sit amet', '<h1>Quae cum essent dicta, finem fecimus et ambulandi et disputandi.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Duo Reges: constructio interrete.</b> Quare conare, quaeso. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. <code>Hos contra singulos dici est melius.</code> Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Gloriosa ostentatio in constituendo summo bono. Sit hoc ultimum bonorum, quod nunc a me defenditur; </p>\r\n\r\n<dl>\r\n	<dt><dfn>Qui convenit?</dfn></dt>\r\n	<dd>De ingenio eius in his disputationibus, non de moribus quaeritur.</dd>\r\n	<dt><dfn>Ita credo.</dfn></dt>\r\n	<dd>Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere posses, si sequerere Pyrrhonem aut Aristonem.</dd>\r\n	<dt><dfn>Quonam, inquit, modo?</dfn></dt>\r\n	<dd>Non enim iam stirpis bonum quaeret, sed animalis.</dd>\r\n	<dt><dfn>Negare non possum.</dfn></dt>\r\n	<dd>Aperiendum est igitur, quid sit voluptas;</dd>\r\n</dl>\r\n\r\n\r\n<ul>\r\n	<li>Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?</li>\r\n	<li>ALIO MODO.</li>\r\n</ul>\r\n\r\n\r\n<p>Suam denique cuique naturam esse ad vivendum ducem. Expectoque quid ad id, quod quaerebam, respondeas. Nam de isto magna dissensio est. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Longum est enim ad omnia respondere, quae a te dicta sunt. Aliter enim explicari, quod quaeritur, non potest. Iam id ipsum absurdum, maximum malum neglegi. <b>Sed tamen intellego quid velit.</b> <a href=\"http://loripsum.net/\" target=\"_blank\">Restatis igitur vos;</a> Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. </p>\r\n\r\n<pre>Nam si beatus umquam fuisset, beatam vitam usque ad illum a\r\nCyro extructum rogum pertulisset.\r\n\r\nCum autem assumpta ratío est, tanto in dominatu locatur, ut\r\nomnia illa prima naturae hulus tutelae subiciantur.\r\n</pre>\r\n\r\n\r\n<ol>\r\n	<li>Atque his de rebus et splendida est eorum et illustris oratio.</li>\r\n	<li>Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.</li>\r\n</ol>\r\n\r\n\r\n<p>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quorum altera prosunt, nocent altera. <b>Easdemne res?</b> In qua si nihil est praeter rationem, sit in una virtute finis bonorum; <a href=\"http://loripsum.net/\" target=\"_blank\">A mene tu?</a> Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. <b>Isto modo ne improbos quidem, si essent boni viri.</b> Deinde disputat, quod cuiusque generis animantium statui deceat extremum. </p>\r\n\r\n<blockquote cite=\"http://loripsum.net\">\r\n	Quae quidem res efficit, ne necesse sit isdem de rebus semper quasi dictata decantare neque a commentariolis suis discedere.\r\n</blockquote>\r\n\r\n\r\n', 'NytZWEJzZ2QvSUU2WWNBMGVZb1d5WWZjbkd6dEZEYmFzL2hIaUxvNUFKcz0=', 'asdasdasdasda', '2018-04-20 18:25:00', 46, NULL, 1, '2018-12-22', '15:59:00', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
