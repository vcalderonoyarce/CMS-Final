<?php

$nivel_base = '../';
require($nivel_base.'init.php');
$accepted_origins = array($_SERVER['PHP_SELF']);
$imageFolder = $nivel_base."../uploads/images/".date('Y').'/'.date('m').'/';
$respFolder = $nivel_base."../../uploads/images/".date('Y').'/'.date('m').'/';
if (!file_exists($imageFolder)) {
    mkdir($imageFolder, 0777, true);
}
reset($_FILES);
$temp = current($_FILES);
if(is_uploaded_file($temp['tmp_name'])){


    // Prohibido subir archivos si el usuario no está logueado
    if($user->usuarioActivo() === false) {
      header("HTTP/1.1 403 Forbidden");
      return;
    }

    if(isset($_SERVER['HTTP_ORIGIN'])){
        // if(in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)){
        //     header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        // }else{
        //     header("HTTP/1.1 403 Origin Denied");
        //     return;
        // }
    }

    // Sanitizar input...
    if(preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])){
        header("HTTP/1.1 400 Invalid file name.");
        return;
    }

    // Verificar extensión
    $extension = pathinfo($temp['name'], PATHINFO_EXTENSION);
    if(!in_array(strtolower($extension), array("gif", "jpg", "png"))){
        header("HTTP/1.1 400 Invalid extension.");
        return;
    }
    // Accept upload if there was no origin, or if it is an accepted origin
    $newFilename = 'img_'.date('Y').date('m').date('d').date('h').date('i').date('s').'.'.$extension;
    $filetowrite = $imageFolder.$newFilename;
    move_uploaded_file($temp['tmp_name'], $filetowrite);
    // Se manda un JSON con la ubicación del archivo subido..
    echo json_encode(array('location' => $respFolder.$newFilename));
} else {
    // Se le indica al editor que se produjo un error al intentar subir el archivo
    header("HTTP/1.1 500 Server Error");
}
?>
