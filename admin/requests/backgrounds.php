<?php

// Fondo del día traído desde la API de Bing
session_start();
// header('Content-Type: application/json');
$json =  file_get_contents('http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=es-CL');
$background = json_decode($json,true);
$fondo = 'http://www.bing.com'.$background['images'][0]['url'];
$_SESSION['bing_background'] = $fondo;

// Control diario, se mantiene en cache 24 horas máximo
header('Pragma: public');
header('Cache-Control: max-age=86400');
header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
header('Content-Type: image/png');
echo file_get_contents($fondo);
?>
