<?php
$nivel_base = '../';
require($nivel_base.'init.php');
$r = null;
try {
  if($user->usuarioActivo()) {
      $notificaciones = new Notificaciones();
      $lista_notificaciones = $notificaciones->cargarNotificaciones($_SESSION['cms_user']);
      $pendientes = $notificaciones->cargarNotificaciones($_SESSION['cms_user'],'pendientes');
      $cuenta_pendientes = is_array($pendientes) ? count($pendientes) : 0;
      $cuenta_total = is_array($lista_notificaciones) ? count($lista_notificaciones) : 0;
      $r['success'] = true;
      $r['message'] = $lang['success_request'];
      $r['data']['total'] = $cuenta_total;
      $r['data']['pending'] = $cuenta_pendientes;
      $r['data']['notifications'] = $lista_notificaciones;


  }
  else {
    throw new \Exception($lang['user_sign_in_warn'], 1);
  }

} catch (\Exception $e) {
  $r['success'] = false;
  $r['message'] = $e->getMessage();
  $r['data'] = null;
}
header("Content-type:application/json");
echo json_encode($r);



?>
