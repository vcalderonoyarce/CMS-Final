<?php

  $nivel_base = '../';
  require($nivel_base.'init.php');
  $r = null;

  try {
    if(!isset($_POST['email']) || strlen(trim($_POST['email'])) == 0 || !isset($_POST['password']) || strlen(trim($_POST['password'])) == 0) {
      throw new \Exception($lang['invalid_request'], 1);
    }
    else {
      $user = $db->runQuery("SELECT id,activo,password FROM cms_usuarios WHERE email = '?'",[htmlentities($_POST['email'])]);
      if(!is_array($user)) {
        throw new \Exception($lang['user_not_exist'], 1);
      }
      else {
        $activo = ($user[0]['activo'] == '1' ? true : false);
        if($activo == false) {
          throw new \Exception($lang['user_not_active'], 1);
        }
        else {
          if(!password_verify($_POST['password'],$user[0]['password'])) {
            throw new \Exception($lang['not_matching_password'], 1);
          }
          else {
            $r['success'] = true;
            $r['data'] = null;
            $r['message'] = $lang['login_successfull'];
            $_SESSION['cms_user'] = $user[0]['id'];
            guardarLog('El usuario con el id '.$_SESSION['cms_user'].' inició sesión desde IP : '.$_SERVER['REMOTE_ADDR']);

          }
        }
      }

    }
  } catch (\Exception $e) {
    $r['success'] = false;
    $r['data'] = null;
    $r['message'] =  $e->getMessage();
  }


  header('Content-Type: application/json');
  echo json_encode($r);



?>
