<?php
  $option = (isset($_GET['option']) && strlen(trim($_GET['option'])) > 0) ? htmlentities($_GET['option']) : 'dashboard';
  $userData = ($user->usuarioActivo() == true) ? $user->obtenerDatos($_SESSION['cms_user']) : null;
?><!DOCTYPE html>
<html <?=($user->usuarioActivo() == false) ? 'class="login-screen perspective"' : 'class="b-'.$option.'"'?>>
  <head>
    <meta charset="utf-8">
    <title>
      <?=$lang['cms_title']?>
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <base href="<?php echo $cms_path; ?>" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#2b3235">
    <script>
      var nivel_base = '<?=$nivel_base?>';
      var system_lang = '<?=$lang['lang_prefix_editor']?>';
      var user_signed_in = <?=$user->usuarioActivo() == true ? 'true' : 'false'?>;
      <?php if($user->usuarioActivo() == true):?>var user_data = <?=json_encode($userData);?>;
      <?php endif;?>
    </script>

    <!-- Styles-->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/open-sans/open-sans.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/font-awesome/css/all.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/tether/dist/css/tether.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/bootstrap/css/bootstrap.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/jquery-ui/jquery-ui.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/sweetalert2/sweetalert2.css" rel="stylesheet" >
    <link href="<?=$nivel_base?>front/css/default.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/jquery.wickedpicker/wickedpicker.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>vendors/jquery.featherlight/release/featherlight.min.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/styles.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/tablet.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/mobile.css<?=caching()?>" rel="stylesheet">
    <link href="<?=$nivel_base?>front/css/printer.css<?=caching()?>" rel="stylesheet" media="print">


    <!-- Scripts -->
    <script src="<?=$nivel_base?>front/js/lang.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/jquery/jquery.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/sweetalert2/sweetalert2.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/tinymce/tinymce.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/tether/dist/js/tether.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/jquery.featherlight/release/featherlight.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/bootstrap/js/bootstrap.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/jquery-ui/jquery-ui.min.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/timeago/jquery.timeago.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>vendors/timeago/locales/jquery.timeago.<?=$lang['lang_prefix']?>.js"></script>
    <script src="<?=$nivel_base?>front/js/scripts.js<?=caching()?>"></script>
    <script src="<?=$nivel_base?>front/js/requests.js<?=caching()?>"></script>
  </head>
  <body class="b-<?=$option?>">
    <?php
      //Renderizador del dashboard completo
      if($user->usuarioActivo() == false) {
        $template = new Plantilla($nivel_base.'system/templates/login.tpl');
        $template->set('lang',$lang);
        echo $template->render();
      }
      else { // Usuario logueado
      ?>
        <?php
        if(file_exists($nivel_base.'system/templates/'.$option.'.tpl')) {
          ?>


            <?php
            $menu = new Plantilla($nivel_base.'system/templates/menu.tpl');
            $menu->set('lang',$lang);
            $menu->set('user_data',$userData);
            echo $menu->render();
            ?>


          <div id="cms-content">
              <?php
              $header = new Plantilla($nivel_base.'system/templates/header.tpl');
              $header->set('lang',$lang);
              $header->set('user_data',$userData);
              echo $header->render();
              ?>
              <div id="main-content">
                  <?php
                  $render = new Plantilla($nivel_base.'system/templates/'.$option.'.tpl');
                  $render->set('cms_path',$cms_path);
                  $render->set('lang',$lang);
                  $render->set('user_data',$userData);
                  $render->set('nivel_base',$nivel_base);
                  echo $render->render();
                  ?>
              </div>
          </div>
          <?php
        }
        else {
          echo '<div id="gradient"></div>';
          $render = new Plantilla($nivel_base.'system/templates/404.tpl');
          $render->set('cms_path',$cms_path);
          $render->set('lang',$lang);
          echo $render->render();
        }

        ?>
      <?php
        $footer = new Plantilla($nivel_base.'system/templates/footer.tpl');
        $footer->set('lang',$lang);
        echo $footer->render();
      ?>
      <?php
      }
    ?>
  </body>
</html>
