<?php
/**
* @version 0.0.1
* @author Víctor Calderón Oyarce victor@ayuda-bloggers.info
*
**/

if(!isset($_SESSION)) {session_start();}
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors',1); // Debug
ini_set('xdebug.var_display_max_depth', 5);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);

if(!isset($nivel_base)) {
  $nivel_base = '';
}

/* Dependencias requeridas */
require($nivel_base.'system/functions.php');
require($nivel_base.'config.php');
require($nivel_base.'system/classes/cms.class/cms.class.php');
require($nivel_base.'system/classes/creditcard.class/creditcard.class.php');
require($nivel_base.'system/classes/encrypt.decrypt.class/encrypt.decrypt.class.php');
require($nivel_base.'system/classes/PHPColors/Color.php');
require($nivel_base.'system/classes/database.class/database.class.php');
require($nivel_base.'system/classes/usuario.class/usuario.class.php');
require($nivel_base.'system/classes/notificaciones.class/notificaciones.class.php');
require($nivel_base.'system/classes/plantilla.class/plantilla.class.php');
require($nivel_base.'system/classes/phpmailer/PHPMailerAutoload.php');
require($nivel_base.'system/classes/email.class/email.class.php');
require($nivel_base.'system/classes/validador.class/validador.class.php');


if(file_exists($nivel_base.'.maintenance') && !isset($maintenance)) {
  $maintenance = true;
}

if(isset($maintenance)) {
  if($maintenance == true) { // Modo mantenimiento -- Para procedimientos de optimización y correcciones
    $template = new Plantilla($nivel_base.'system/templates/maintenance.tpl');
    $template->set('lang',$lang);
    $template->set('cms_path',$cms_path);
    $template->set('nivel_base',$nivel_base);
    echo $template->render();
    exit();
  }
}


$db = new Database();
$user = new Usuario();
$cms = new CMS();

if($db->connect() == false) {
  $template = new Plantilla($nivel_base.'system/templates/db_connection.tpl');
  $template->set('error_connection_title',$lang['error_connection_title']);
  $template->set('error_connection_body',$lang['error_connection_body']);
  echo $template->render();
  exit();
}
$db->disconnect();
