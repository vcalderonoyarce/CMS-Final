# Sistema de gestión de contenidos
* Creador y mantenedor: Víctor Calderón Oyarce
* Correo electrónico: vcalderonoyarce@gmail.com
* Teléfono: +569 6150 2019
* Estado: EN CONSTRUCCIÓN.

*Este repositorio contiene material preliminar con el fin de almacenar toda la información, documentación y código del mismo.*

Este CMS fue desarrollado para la creación de sitios web, para simplicar su desarrollo y así otorgar al desarrollador una herramienta Back-End sin necesidad de programarla.

## Requisitos

* PHP >= 5.6

# Documentación [![Inline docs](http://inch-ci.org/github/vcalderonoyarce/CMS-FINAL.svg?branch=master)](http://inch-ci.org/github/vcalderonoyarce/CMS-FINAL)

**Toda la documentación puede encontrarse dentro de la carpeta docs/**

Prohibida su reproducción parcial o total.

[![HitCount](http://hits.dwyl.com/vcalderonoyarce/CMS-FINAL.svg)](http://hits.dwyl.com/vcalderonoyarce/CMS-FINAL)
